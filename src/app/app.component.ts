import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Portail Volleynews.be';

  constructor(private router: Router) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        console.log('subscribe router.events');
        (<any>window).ga('set', 'page', 'portail' + event.urlAfterRedirects);
        (<any>window).ga('send', 'pageview');
      }
    });
  }
}
