import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotImplementedPageComponent } from 'src/app/components/pages/not-implemented-page/not-implemented-page.component';
import { NotFoundPageComponent } from 'src/app/components/pages/not-found-page/not-found-page.component';
import { RankingPageComponent } from './ranking/ranking-page/ranking-page.component';
import { HomeDashboardComponent } from 'src/app/components/pages/home-dashboard/home-dashboard.component';
import { MainContentComponent } from 'src/app/components/pages/main-content-page/main-content-page.component';
import { CalendarWidgetComponent } from 'src/app/components/widgets/calendar-widget/calendar-widget.component';
import { RankingWidgetComponent } from 'src/app/components/widgets/ranking-widget/ranking-widget.component';
import { DistrictSelectorComponent } from 'src/app/components/selectors/district-selector/district-selector.component';
import { DocumentationWidgetComponent } from 'src/app/components/widgets/documentation-widget/documentation-widget.component';
import { LinkPageComponent } from './components/pages/link-page/link-page.component';
import { SynopticTableWidgetComponent } from './components/widgets/synoptic-table-widget/synoptic-table-widget.component';
import { AboutPageComponent } from './components/pages/about-page/about-page.component';

const routes: Routes = [
  {
    path: 'widget/calendar',
    component: CalendarWidgetComponent
  },
  {
    path: 'widget/ranking',
    component: RankingWidgetComponent
  },
  {
    path: 'widget/documentation',
    component: DocumentationWidgetComponent
  },
  {
    path: 'widget/synoptic',
    component: SynopticTableWidgetComponent
  },
  {
    path: '',
    component: MainContentComponent,
    children: [
      {
        path: 'dashboard',
        component: HomeDashboardComponent
      },
      {
        path: 'calendar',
        component: DistrictSelectorComponent
      },
      {
        path: 'ranking',
        component: RankingPageComponent
      },
      {
        path: 'cups',
        component: NotImplementedPageComponent
      },
      {
        path: 'clubs',
        component: NotImplementedPageComponent
      },
      {
        path: 'coaches',
        component: NotImplementedPageComponent
      },
      {
        path: 'players',
        component: NotImplementedPageComponent
      },
      {
        path: 'venues',
        component: NotImplementedPageComponent
      },
      {
        path: 'referees',
        component: NotImplementedPageComponent
      },
      {
        path: 'links',
        component: LinkPageComponent
      },
      {
        path: 'about',
        component: AboutPageComponent
      },
      { path: '**', component: HomeDashboardComponent }
    ]
  },

  {
    path: '',
    component: NotImplementedPageComponent
  },
  { path: '**', component: NotFoundPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {}
