import { Component, OnInit } from '@angular/core';
import { FvwbQueryFilter } from 'src/app/models/ApiQueryDescriptor';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-calendar-widget',
  templateUrl: './calendar-widget.component.html',
  styleUrls: ['./calendar-widget.component.scss']
})
export class CalendarWidgetComponent implements OnInit {
  public queryFilter: FvwbQueryFilter;

  constructor(private activatedRoute: ActivatedRoute) {
    this.queryFilter = <FvwbQueryFilter>{};
    this.activatedRoute.queryParams.subscribe(params => {
      this.queryFilter.championshipId = params['championshipId'];
    });
  }

  ngOnInit() {}
}
