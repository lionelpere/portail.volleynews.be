import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FvwbQueryFilter } from 'src/app/models/ApiQueryDescriptor';

@Component({
  selector: 'app-synoptic-table-widget',
  templateUrl: './synoptic-table-widget.component.html',
  styleUrls: ['./synoptic-table-widget.component.scss']
})
export class SynopticTableWidgetComponent implements OnInit {
  public queryFilter: FvwbQueryFilter;

  constructor(private activatedRoute: ActivatedRoute) {
    this.queryFilter = <FvwbQueryFilter>{};
    this.activatedRoute.queryParams.subscribe(params => {
      this.queryFilter.championshipId = params['championshipId'];
    });
  }

  ngOnInit() {}
}
