import { Component, OnInit } from '@angular/core';
import { FvwbQueryFilter, FvwbDataFilter } from 'src/app/models/ApiQueryDescriptor';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ranking-widget',
  templateUrl: './ranking-widget.component.html',
  styleUrls: ['./ranking-widget.component.scss']
})
export class RankingWidgetComponent implements OnInit {
  public queryFilter: FvwbQueryFilter;
  public dataFilter: FvwbDataFilter;

  constructor(private activatedRoute: ActivatedRoute) {
    this.queryFilter = <FvwbQueryFilter>{};
    this.dataFilter = <FvwbDataFilter>{};
    this.activatedRoute.queryParams.subscribe(params => {
      this.queryFilter.championshipId = params['championshipId'];
      this.dataFilter.teamId = params['teamId'];
    });
  }

  ngOnInit() {}
}
