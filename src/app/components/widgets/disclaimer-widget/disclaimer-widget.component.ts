import { Component, OnInit, Input } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-disclaimer-widget',
  templateUrl: './disclaimer-widget.component.html',
  styleUrls: ['./disclaimer-widget.component.scss']
})
export class DisclaimerWidgetComponent implements OnInit {
  public hideDisclaimerValue: boolean;
  public isPermanent: boolean;

  @Input()
  public set allwaysVisible(allwaysVisible: boolean) {
    console.log('input set', this.isPermanent);
    this.isPermanent = allwaysVisible;
    console.log('input set end', this.isPermanent);
  }

  constructor(private localStorageService: LocalStorageService) {
    // this.hideDisclaimerValue = localStorageService.get('hideDisclaimer');
    console.log('constructor', this.isPermanent);
    this.isPermanent = false;
  }

  ngOnInit() {}

  hideDisclaimer() {
    this.hideDisclaimerValue = true;
    this.localStorageService.set('hideDisclaimer', true);
    console.log('wtf 1');
    this.localStorageService.set('hideDisclaimerDate', new Date().getTime());
  }

  isDisclaimerVisible(): boolean {
    const hide = this.localStorageService.get('hideDisclaimer');
    if (hide) {
      const hideDate: number = this.localStorageService.get('hideDisclaimerDate');
      if (hideDate) {
        const dtNow: Date = new Date();
        const elapsedTimeInMs: any = dtNow.getTime() - hideDate;
        const ttl = 7 * 24 * 60 * 60 * 1000; // Display every week
        if (elapsedTimeInMs > ttl) {
          this.localStorageService.set('hideDisclaimer', false);
          this.localStorageService.set('hideDisclaimerDate', null);
        }
      } else {
        console.log('wtf 1');
        this.localStorageService.set('hideDisclaimerDate', new Date().getTime());
      }
    }
    return !this.localStorageService.get('hideDisclaimer');
  }
}
