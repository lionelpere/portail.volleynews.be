import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import {
  FvwbQueryFilter,
  FvwbQuery,
  FvwbQueryType,
  FvwbDataFilter
} from 'src/app/models/ApiQueryDescriptor';
import { PagingRankingData, PagingRankingResult } from 'src/app/models/ApiResultModels';
import { Subscription, Subject } from 'rxjs';
import { GridOptions, ColDef, ColGroupDef, GridReadyEvent, RowNode } from 'ag-grid';
import { GridActionsDef } from 'src/app/toolbox-ui/grid/models/grid-actions-def';
import { FvwbApiService } from 'src/app/services/fvwb-api.service';
import { DataUtilsService } from 'src/app/services/data-utils.service';
import { switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-grid-ranking',
  templateUrl: './grid-ranking.component.html',
  styleUrls: ['./grid-ranking.component.scss']
})
export class GridRankingComponent implements OnInit, OnDestroy {
  private _rankingQuery: FvwbQuery = <FvwbQuery>{ type: FvwbQueryType.PagingRanking };
  private _subscriptions: Subscription[] = [];
  private _$reloadSubject: Subject<any> = new Subject<any>();
  private _dataFilter: FvwbDataFilter;

  public gridData: PagingRankingData[] = [];
  public loading: boolean;
  public isReserveRanking: boolean;
  public calculatedHeight: number;

  @Input()
  public set isReserve(isReserve: boolean) {
    this._rankingQuery.type = isReserve
      ? FvwbQueryType.PagingRankingReserve
      : FvwbQueryType.PagingRanking;
    this.isReserveRanking = isReserve;
  }

  @Input()
  public set queryFilter(queryFilter: FvwbQueryFilter) {
    this._rankingQuery.filter = queryFilter;
  }

  @Input()
  public set dataFilter(dataFilter: FvwbDataFilter) {
    this._dataFilter = dataFilter;
  }

  // grid parameters
  public gridOptions: GridOptions;
  public gridActionsDef: GridActionsDef = {};
  public gridColumnDefs: (ColDef | ColGroupDef)[] = [];

  constructor(private apiService: FvwbApiService, private dataUtilsService: DataUtilsService) {
    this.loading = true;
    this.calculatedHeight = 400;

    this.gridColumnDefs = [
      {
        field: 'Position',
        minWidth: 45,
        maxWidth: 45,
        headerName: '#',
        sort: 'asc',
        suppressMovable: true,
        suppressSorting: true,
        suppressFilter: true
      },
      {
        field: 'TeamName',
        minWidth: 150,
        headerName: 'Equipe',
        suppressMovable: true,
        suppressSorting: true,
        suppressFilter: true
      },
      {
        field: 'MatchPlayed',
        minWidth: 55,
        maxWidth: 55,
        headerName: 'M',
        suppressMovable: true,
        suppressSorting: true,
        suppressFilter: true,
        suppressMenu: true
      },
      {
        field: 'Winner3Points',
        minWidth: 59,
        maxWidth: 59,
        headerName: 'W3',
        suppressMovable: true
      },
      {
        field: 'Winner2Points',
        minWidth: 59,
        maxWidth: 59,
        headerName: 'W2',
        suppressMovable: true
      },
      {
        field: 'Looser1Point',
        minWidth: 54,
        maxWidth: 54,
        headerName: 'L1',
        suppressMovable: true
      },
      {
        field: 'Looser0Point',
        minWidth: 53,
        maxWidth: 53,
        headerName: 'L0',
        suppressMovable: true,
        headerClass: 'header-text-center'
      },
      {
        field: 'Points',
        minWidth: 60,
        maxWidth: 60,
        headerName: 'P+',
        suppressMovable: true
      },
      {
        field: 'PointsMinus',
        minWidth: 60,
        maxWidth: 60,
        headerName: 'P-',
        suppressMovable: true
      }
    ];

    this.gridOptions = {
      columnDefs: this.gridColumnDefs
    };
  }

  ngOnInit() {
    const $championships = this._$reloadSubject.pipe(
      switchMap(_ =>
        this.apiService.queryFvwbApi<PagingRankingResult>(this._rankingQuery).pipe(
          tap(d => {
            const data = (<PagingRankingResult>d).Data;
            this.calculatedHeight = 50 + 28 * data.length;
            // if (this._dataFilter && this._dataFilter.teamId) {
            // } else {
            //   this.gridData = data;
            //   this.gridOptions.api.forEachNode = function(node) {
            //     if (node.data.teamId === this._dataFilter.teamId) {
            //       this.gridOptions.api.selectNode(node, true);
            //     }
            //   };
            // }
            this.gridData = data;
            console.log('gridData', data);
            this.loading = false;
          })
        )
      )
    );

    this._subscriptions = [...this._subscriptions, $championships.subscribe()];
    this.refresh();
  }

  onGridReady(event: GridReadyEvent): void {
    if (this._dataFilter && this._dataFilter.teamId) {
      console.log('gridReady', event);
      event.api.forEachNode(function(node) {
        console.log('forEachNode', node);
        if ((<RowNode>node).data.TeamId === this._dataFilter.teamId) {
          console.log('node found', node);
          event.api.selectNode(node, true);
        } else {
          console.log('node invalid', node);
        }
      });
    }
    event.api.sizeColumnsToFit();
  }

  public onRowDataChanged(): void {
    if (this._dataFilter && this._dataFilter.teamId) {
      console.log('rowDataChanged', event);
      this.gridOptions.api.forEachNode(function(node) {
        console.log('forEachNode', node);
        if ((<RowNode>node).data.TeamId === this._dataFilter.teamId) {
          console.log('node found', node);
          this.gridOptions.api.selectNode(node, true);
        } else {
          console.log('node invalid', node);
        }
      });
    }
  }

  refresh() {
    this.loading = true;
    this._$reloadSubject.next(null);
  }

  getTitle(): string {
    if (this.isReserveRanking) {
      return 'Classement réserve';
    }
    return 'Classement';
  }
  public ngOnDestroy(): void {
    for (const sub of this._subscriptions) {
      sub.unsubscribe();
    }
  }
}
