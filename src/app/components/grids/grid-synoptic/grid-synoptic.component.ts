import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { tap, switchMap } from 'rxjs/operators';
import { DataUtilsService } from 'src/app/services/data-utils.service';
import { GridReadyEvent, GridOptions, ColDef, ColGroupDef } from 'ag-grid';
import { GridActionsDef } from 'src/app/toolbox-ui/grid/models/grid-actions-def';
import {
  FvwbQuery,
  FvwbQueryFilter,
  FvwbQueryType,
  FvwbDataFilter
} from 'src/app/models/ApiQueryDescriptor';
import {
  PagingCalendarData,
  PagingCalendarResult,
  PagingSynopticTableResult,
  PagingSynopticTableData
} from 'src/app/models/ApiResultModels';
import { FvwbApiService } from 'src/app/services/fvwb-api.service';

@Component({
  selector: 'app-grid-synoptic',
  templateUrl: './grid-synoptic.component.html',
  styleUrls: ['./grid-synoptic.component.scss']
})
export class GridSynopticComponent implements OnInit, OnDestroy {
  private _calendarQuery: FvwbQuery;
  private _subscriptions: Subscription[] = [];
  private _$reloadSubject: Subject<any> = new Subject<any>();
  private _dataFilter: FvwbDataFilter;

  public gridData: PagingSynopticTableData[] = [];
  public calculatedHeight: number;
  public loading: boolean;

  @Input()
  public set queryFilter(queryFilter: FvwbQueryFilter) {
    this._calendarQuery = <FvwbQuery>{
      type: FvwbQueryType.PagingSynopticTable,
      filter: queryFilter
    };
  }

  @Input()
  public set dataFilter(dataFilter: FvwbDataFilter) {
    this._dataFilter = dataFilter;
  }

  // grid parameters
  public gridOptions: GridOptions;
  public gridActionsDef: GridActionsDef = {};
  public gridColumnDefs: (ColDef | ColGroupDef)[] = [];

  constructor(private apiService: FvwbApiService, private dataUtilsService: DataUtilsService) {
    this.loading = true;
    this.calculatedHeight = 400;
  }

  ngOnInit() {
    this.gridColumnDefs = [
      {
        field: 'Equipes',
        minWidth: 75,
        headerName: 'Date',
        sort: 'asc',
        suppressMovable: true
      }
    ];
    this.gridOptions = {
      columnDefs: this.gridColumnDefs
    };

    const $championships = this._$reloadSubject.pipe(
      switchMap(_ =>
        this.apiService.queryFvwbApi<PagingSynopticTableResult>(this._calendarQuery).pipe(
          tap(d => {
            const data = (<PagingSynopticTableResult>d).Data;
            this.gridOptions = {
              columnDefs: this.generateGridColumnsDef(data[0])
            };
            this.calculatedHeight = 50 + 28 * data.length;
            this.gridData = data;
            this.loading = false;
          })
        )
      )
    );

    this._subscriptions = [...this._subscriptions, $championships.subscribe()];
    this.refresh();
  }

  generateGridColumnsDef(data: PagingSynopticTableData): ColDef[] {
    const columnDefs: ColDef[] = [];
    columnDefs.push(<ColDef>{
      field: 'Equipes',
      minWidth: 75,
      headerName: 'Equipes',
      suppressSorting: true,
      suppressFilter: true,
      suppressMovable: true
    });

    for (const key in data) {
      if (key !== 'Equipes') {
        columnDefs.push(<ColDef>{
          field: key,
          minWidth: 80,
          maxWidth: 80,
          headerName: key,
          suppressSorting: true,
          suppressFilter: true,
          suppressMovable: true
        });
      }
    }

    return columnDefs;
  }

  onGridReady(event: GridReadyEvent): void {
    event.api.sizeColumnsToFit();
  }

  refresh() {
    this.loading = true;
    this._$reloadSubject.next(null);
  }
  public ngOnDestroy(): void {
    for (const sub of this._subscriptions) {
      sub.unsubscribe();
    }
  }
}
