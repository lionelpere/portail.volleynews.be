import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { tap, switchMap } from 'rxjs/operators';
import { DataUtilsService } from 'src/app/services/data-utils.service';
import { GridReadyEvent, GridOptions, ColDef, ColGroupDef } from 'ag-grid';
import { GridActionsDef } from 'src/app/toolbox-ui/grid/models/grid-actions-def';
import {
  FvwbQuery,
  FvwbQueryFilter,
  FvwbQueryType,
  FvwbDataFilter
} from 'src/app/models/ApiQueryDescriptor';
import { PagingCalendarData, PagingCalendarResult } from 'src/app/models/ApiResultModels';
import { FvwbApiService } from 'src/app/services/fvwb-api.service';
import * as moment from 'moment';

@Component({
  selector: 'app-grid-calendar',
  templateUrl: './grid-calendar.component.html',
  styleUrls: ['./grid-calendar.component.scss']
})
export class GridCalendarComponent implements OnInit, OnDestroy {
  private _calendarQuery: FvwbQuery;
  private _subscriptions: Subscription[] = [];
  private _$reloadSubject: Subject<any> = new Subject<any>();

  public gridData: PagingCalendarData[] = [];
  public loading: boolean;
  public calendarTitle: string;
  private _dataFilter: FvwbDataFilter;
  public noFilter: boolean;
  public calculatedHeight: number;

  @Input()
  public set queryFilter(queryFilter: FvwbQueryFilter) {
    this._calendarQuery = <FvwbQuery>{
      type: FvwbQueryType.PagingCalendar,
      filter: queryFilter
    };
  }

  @Input()
  public set dataFilter(dataFilter: FvwbDataFilter) {
    this._dataFilter = dataFilter;
  }

  // grid parameters
  public gridOptions: GridOptions;
  public gridActionsDef: GridActionsDef = {};
  public gridColumnDefs: (ColDef | ColGroupDef)[] = [];

  constructor(private apiService: FvwbApiService, private dataUtilsService: DataUtilsService) {
    this.loading = true;
    this.calendarTitle = 'Calendrier et résultats';
    this.calculatedHeight = 600;
  }

  ngOnInit() {
    if (!!this._dataFilter) {
      console.log('dateFilter', this._dataFilter);
      this.noFilter = true;
      this.calendarTitle =
        'Derniers résultats et prochaines rencontres' +
        ' (du ' +
        this._dataFilter.dateFrom.format('DD/MM') +
        ' au ' +
        this._dataFilter.dateTo.format('DD/MM') +
        ')';
    }

    this.gridColumnDefs = [
      {
        field: 'CalendarDateDate',
        minWidth: 90,
        maxWidth: 90,
        headerName: 'Date',
        sort: 'asc',
        valueFormatter: function formatDate(params) {
          return params.data.CalendarDateString;
        },
        suppressMovable: true
      },
      {
        field: 'CalendarHourDate',
        headerName: 'Heure',
        minWidth: 80,
        maxWidth: 80,
        valueFormatter: function formatHour(params) {
          return params.data.CalendarHourString;
        },
        suppressMovable: true
      },
      {
        field: 'HomeTeam',
        headerName: 'Domicile',
        minWidth: 180,
        suppressMovable: true
      },
      {
        field: 'AwayTeam',
        headerName: 'Visiteur',
        minWidth: 180,
        suppressMovable: true
      },
      {
        field: 'Score',
        headerName: 'Score',
        minWidth: 180,
        maxWidth: 240,
        valueFormatter: function formatScore(params) {
          if (!!params.data.ScoreDetail) {
            return params.data.Score + ' (' + params.data.ScoreDetail + ')';
          } else {
            return params.value;
          }
        },
        suppressMovable: true
      },
      {
        field: 'ScoreRes',
        headerName: 'Score Réserve',
        minWidth: 120,
        maxWidth: 130,
        suppressMovable: true
      }
    ];
    this.gridOptions = {
      columnDefs: this.gridColumnDefs
    };

    const $championships = this._$reloadSubject.pipe(
      switchMap(_ =>
        this.apiService.queryFvwbApi<PagingCalendarResult>(this._calendarQuery).pipe(
          tap(d => {
            let data = (<PagingCalendarResult>d).Data;
            data = data.filter(c => c.ChampionshipId !== 0).map(c => {
              c.CalendarHourDate = this.dataUtilsService.getDateFromString(c.CalendarHour);
              c.CalendarHourString = this.dataUtilsService.getHourStringFromDate(
                c.CalendarHourDate
              );
              c.CalendarDateDate = this.dataUtilsService.getDateFromString(c.CalendarDate);
              c.CalendarDateString = this.dataUtilsService.getFrenchDateStringFromDate(
                c.CalendarDateDate
              );
              return c;
            });
            if (!!this._dataFilter) {
              data = data.filter(c => {
                const currentDate = moment(c.CalendarDateDate);
                return (
                  currentDate > this._dataFilter.dateFrom && currentDate < this._dataFilter.dateTo
                );
              });
            }
            this.calculatedHeight = 50 + 29 * data.length;
            this.gridData = data;
            this.loading = false;
          })
        )
      )
    );

    this._subscriptions = [...this._subscriptions, $championships.subscribe()];
    this.refresh();
  }

  onGridReady(event: GridReadyEvent): void {
    event.api.sizeColumnsToFit();
  }

  refresh() {
    this.loading = true;
    this._$reloadSubject.next(null);
  }
  public ngOnDestroy(): void {
    for (const sub of this._subscriptions) {
      sub.unsubscribe();
    }
  }
}
