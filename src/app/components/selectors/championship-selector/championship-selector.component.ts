import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { DataUtilsService } from 'src/app/services/data-utils.service';
import { FvwbApiService } from 'src/app/services/fvwb-api.service';
import {
  FvwbQuery,
  FvwbQueryFilter,
  FvwbQueryType,
  FvwbDataFilter
} from 'src/app/models/ApiQueryDescriptor';
import { PagingRankingChampionshipResult, ChampionshipData } from 'src/app/models/ApiResultModels';
import * as moment from 'moment';
@Component({
  selector: 'app-championship-selector',
  templateUrl: './championship-selector.component.html',
  styleUrls: ['./championship-selector.component.scss']
})
export class ChampionshipSelectorComponent implements OnInit, OnDestroy {
  private _disctrictId: number;
  private _subscriptions: Subscription[] = [];
  public championshipList: ChampionshipData[] = [];
  public _queryType: FvwbQueryType;
  public fvwbQueryType: typeof FvwbQueryType = FvwbQueryType;
  public recentDataFilter: FvwbDataFilter;

  @Input()
  public set districtId(districtId: number) {
    this._disctrictId = districtId;
  }

  @Input()
  public set queryType(queryType: number) {
    this._queryType = queryType;
  }

  constructor(private apiService: FvwbApiService, private dataUtilsService: DataUtilsService) {
    this.recentDataFilter = <FvwbDataFilter>{
      dateFrom: moment().add(-1, 'week'),
      dateTo: moment().add(1, 'week')
    };
  }

  ngOnInit() {
    const fvwbQuery = <FvwbQuery>{
      type: FvwbQueryType.PagingRankingChampionship,
      filter: <FvwbQueryFilter>{ districtId: this._disctrictId }
    };
    const $championships = this.apiService
      .queryFvwbApi<PagingRankingChampionshipResult>(fvwbQuery)
      .pipe(
        tap(d => {
          this.championshipList = d.Data.filter(c => c.ChampionshipId !== 0);
        })
      );

    this._subscriptions = [...this._subscriptions, $championships.subscribe()];
  }

  shortenChampionshipName(name: string): string {
    return this.dataUtilsService.getShortChampionshipName(name);
  }

  getQueryFilter(championshipId: number): FvwbQueryFilter {
    return <FvwbQueryFilter>{ championshipId: championshipId };
  }

  public ngOnDestroy(): void {
    for (const sub of this._subscriptions) {
      sub.unsubscribe();
    }
  }
}
