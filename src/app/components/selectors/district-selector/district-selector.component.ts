import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription, combineLatest, BehaviorSubject, merge, empty, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { DisctrictDDL } from 'src/app/models/ApiResultModels';
import { FvwbQueryType, FvwbQuery, FvwbQueryResult } from 'src/app/models/ApiQueryDescriptor';
import { FvwbApiService } from 'src/app/services/fvwb-api.service';

@Component({
  selector: 'app-district-selector',
  templateUrl: './district-selector.component.html',
  styleUrls: ['./district-selector.component.scss']
})
export class DistrictSelectorComponent implements OnInit, OnDestroy {
  public districtList: DisctrictDDL[] = [];
  private _subscriptions: Subscription[] = [];
  public _queryType: FvwbQueryType;
  public fvwbQueryType: typeof FvwbQueryType = FvwbQueryType;

  @Input()
  public set queryType(queryType: number) {
    this._queryType = queryType;
  }

  constructor(private apiService: FvwbApiService) {}

  ngOnInit() {
    const fvwbQuery = <FvwbQuery>{
      type: FvwbQueryType.GetDistrictDDL
    };

    const $districts = this.apiService.queryFvwbApi<FvwbQueryResult>(fvwbQuery).pipe(
      tap(d => {
        this.districtList = (<DisctrictDDL[]>d.Data).filter(dis => dis.Id !== 0);
      })
    );

    this._subscriptions = [...this._subscriptions, $districts.subscribe()];
  }

  public ngOnDestroy(): void {
    for (const sub of this._subscriptions) {
      sub.unsubscribe();
    }
  }
}
