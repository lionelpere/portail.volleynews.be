import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginBoxComponent } from './login-box/login-box.component';
import { LoginBoxHeaderComponent } from './login-box-header/login-box-header.component';
import { LoginBoxContentComponent } from './login-box-content/login-box-content.component';
import { LoginBoxFooterComponent } from './login-box-footer/login-box-footer.component';
import { LoginBoxContentLogoComponent } from './login-box-content-logo/login-box-content-logo.component';
import { LoginBoxContentInputComponent } from './login-box-content-input/login-box-content-input.component';
import { ReactiveFormsModule } from '@angular/forms';
import { IconsModule } from '../icons/icons.module';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, IconsModule],
  declarations: [
    LoginBoxComponent,
    LoginBoxHeaderComponent,
    LoginBoxContentComponent,
    LoginBoxFooterComponent,
    LoginBoxContentLogoComponent,
    LoginBoxContentInputComponent
  ],
  exports: [
    LoginBoxComponent,
    LoginBoxHeaderComponent,
    LoginBoxContentComponent,
    LoginBoxFooterComponent,
    LoginBoxContentLogoComponent,
    LoginBoxContentInputComponent
  ]
})
export class LoginBoxModule {}
