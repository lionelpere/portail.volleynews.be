import { Component, OnInit, Input } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-login-box-content-logo',
  templateUrl: './login-box-content-logo.component.html',
  styleUrls: ['./login-box-content-logo.component.scss']
})
export class LoginBoxContentLogoComponent implements OnInit {
  @Input() logoSrc: string;
  constructor() { }

  ngOnInit() {
  }

}
