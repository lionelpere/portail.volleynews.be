import {
  Component,
  OnInit,
  Input,
  ViewChild,
  forwardRef,
  Renderer2,
  ElementRef
} from '@angular/core';
import { InputType } from '../../enums/input-type.enum';
import { InputStyle } from './login-box-content-input-style.enum';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => LoginBoxContentInputComponent),
  multi: true
};

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-login-box-content-input',
  templateUrl: './login-box-content-input.component.html',
  styleUrls: ['./login-box-content-input.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class LoginBoxContentInputComponent
  implements OnInit, ControlValueAccessor {
  @ViewChild('input') input: ElementRef;
  @Input() label: string;
  @Input() icon: string;
  @Input() tooltip: string;
  @Input() errorTxt: string;
  @Input() inputType: InputType;
  @Input() inputStyle: InputStyle;
  @Input() placeholder: string;
  public InputStyle: typeof InputStyle = InputStyle;
  public inputTypeConverted: string;

  private _modelValue: any;
  public changed: Array<(data: any) => void> = new Array<
    (value: any) => void
  >();
  public touched: Array<() => void> = new Array<() => void>();
  public isDisabled: boolean;

  constructor(private _renderer: Renderer2) {
    this.isDisabled = false;
    this.placeholder = '';
  }

  ngOnInit() {
    if (typeof this.inputType === 'undefined' || this.inputType === null) {
      throw new Error('Attribute "InputType" is required');
    }
    if (typeof this.inputStyle === 'undefined' || this.inputStyle === null) {
      this.inputStyle = InputStyle.Classic;
    }
    switch (this.inputType) {
      case InputType.Text:
        this.inputTypeConverted = 'text';
        break;
      case InputType.Password:
        this.inputTypeConverted = 'password';
        break;
      default:
        throw new Error('Attribute "InputType" set to an unhandled value !');
    }
  }

  Touched() {
    this.touched.forEach(_ => _());
  }

  Change($event) {
    this._modelValue = $event.target.value;
    this.changed.forEach(_ => _($event.target.value));
  }

  writeValue(value: any): void {
    this._modelValue = value;
    this._renderer.setAttribute(this.input.nativeElement, 'value', value);
  }
  registerOnChange(fn: (value: any) => void): void {
    this.changed.push(fn);
  }
  registerOnTouched(fn: () => void): void {
    this.touched.push(fn);
  }
  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
