import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Input,
  Output,
  EventEmitter,
  AfterViewInit,
  AfterContentChecked
} from '@angular/core';

import * as $ from 'jquery';
import { differenceWith, isEqual, differenceBy } from 'lodash';
import 'fullcalendar';
import 'fullcalendar-scheduler';
import { BusinessHoursInput, EventObjectInput, OptionsInput } from 'fullcalendar';
import { CalendarView } from '../calendar-view.enum';
import { Time } from '@angular/common';
import { EventSourceInput } from 'fullcalendar/src/types/input-types';
import { ResourceInput } from 'fullcalendar-scheduler/src/exports';
import { Moment } from 'moment';

const calendarDefaultOptions: OptionsInput = {
  schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
  locale: 'en',
  themeSystem: 'bootstrap4',
  defaultView: 'month',
  slotLabelFormat: 'H:mm',
  timeFormat: 'H:mm',
  allDaySlot: false,
  weekends: false,
  header: {
    left: 'title',
    right: 'today,prev,next'
  },
  buttonIcons: false,
  themeButtonIcons: false,
  buttonText: {
    today: 'today',
    month: 'month',
    week: 'week',
    day: 'day',
    list: 'list'
  },
  contentHeight: 'auto'
  // views: {
  //   agenda: {
  //     allDaySlot: false,
  //     weekends: false
  //   }
  // }
};

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit, AfterContentChecked {
  @ViewChild('calendar') calendar: ElementRef;
  @Output('eventClick')
  eventClick: EventEmitter<EventObjectInput> = new EventEmitter<EventObjectInput>();
  @Input('events')
  public set events(events: EventObjectInput[]) {
    this.refreshEvents(events.map(e => ({ ...e })));
  }
  @Input('locale')
  public set locale(lang: string) {
    this.refreshCalendar({ ...this._currentOption, locale: lang });
  }
  @Input('header')
  public set header(val: Object | boolean) {
    this.refreshCalendar({ ...this._currentOption, header: val });
  }
  @Input('columnHeaderFormat')
  public set columnHeaderFormat(format: string) {
    this.refreshCalendar({ ...this._currentOption, columnHeaderFormat: format });
  }
  // @Input('buttonText')
  // public set buttonText(buttonTexts: any) {
  //   this.refreshCalendar(Object.assign({}, this._currentOption, { buttonText: buttonTexts }));
  // }
  @Input('view')
  public set view(view: CalendarView) {
    this.refreshCalendar({
      ...this._currentOption,
      defaultView: CalendarView[view]
    });
  }
  @Input('timeLabelFormat')
  public set timeLabelFormat(format: string) {
    this.refreshCalendar({ ...this._currentOption, slotLabelFormat: format });
  }
  @Input('minTime')
  public set minTime(time: string) {
    this.refreshCalendar({ ...this._currentOption, minTime: time });
  }
  @Input('maxTime')
  public set maxTime(time: string) {
    this.refreshCalendar({ ...this._currentOption, maxTime: time });
  }
  @Input('businessHours')
  public set businessHours(businessHours: BusinessHoursInput | BusinessHoursInput[]) {
    this.refreshCalendar({ ...this._currentOption, businessHours: businessHours });
  }
  @Input('showWeekends')
  public set showWeekends(showWeekends: boolean) {
    this.refreshCalendar({ ...this._currentOption, weekends: showWeekends });
  }
  @Input('resources')
  public set resources(resources: ResourceInput[]) {
    this.refreshResources(resources.map(r => ({ ...r })));
  }
  @Input('groupByResource')
  public set groupByResource(groupByResource: boolean) {
    this.refreshCalendar({
      ...this._currentOption,
      groupByResource: groupByResource
    });
  }
  @Input('showAllDaySlot')
  public set showAllDaySlot(showAllDaySlot: boolean) {
    this.refreshCalendar({
      ...this._currentOption,
      allDaySlot: showAllDaySlot
    });
  }
  @Input('slotEventOverlap')
  public set slotEventOverlap(overlap: boolean) {
    this.refreshCalendar({ ...this._currentOption, slotEventOverlap: overlap });
  }
  @Input('allDaySlotLabel')
  public set allDaySlotLabel(allDaySlotLabel: string) {
    this.refreshCalendar({ ...this._currentOption, allDayText: allDaySlotLabel });
  }
  @Input('datePosition')
  public set datePosition(datePosition: string) {
    this.refreshDatePosition(datePosition);
  }

  @Input() public showToday: boolean;

  private _currentOption: OptionsInput;
  private _events: EventObjectInput[];
  private _resources: ResourceInput[];
  private _datePosition: string;
  private _calendarRef: JQuery;
  private _visible: boolean;

  private get calendarOptions(): OptionsInput {
    return {
      ...calendarDefaultOptions,
      ...this._currentOption,
      eventClick: this.Click.bind(this),
      events: this.loadEvents.bind(this),
      resources: this.loadResources.bind(this)
    };
  }

  constructor() {
    this.showToday = true;
    this._events = [];
    this._resources = [];
    this._visible = false;
  }

  ngOnInit(): void {
    this._calendarRef = $(this.calendar.nativeElement);
    this._calendarRef.fullCalendar(this.calendarOptions);
    this.refreshDatePosition(this._datePosition);
  }

  ngAfterContentChecked(): void {
    if (this._visible === false && this.calendar.nativeElement.offsetParent !== null) {
      this._visible = true;
      this._calendarRef.fullCalendar('render');
      this._calendarRef.fullCalendar('next');
      this._calendarRef.fullCalendar('prev');
    } else if (this._visible === true && this.calendar.nativeElement.offsetParent === null) {
      this._visible = false;
    }
  }

  private Click(event: EventObjectInput) {
    this.eventClick.emit(event);
  }

  private loadEvents(s, e, t, cb) {
    cb(this._events);
  }

  private loadResources(cb, s, e, tz) {
    cb(this._resources);
  }

  private refreshCalendar(options: OptionsInput) {
    if (typeof this._calendarRef !== 'undefined') {
      if (this._currentOption.locale !== options.locale) {
        this._calendarRef.fullCalendar('option', 'locale', options.locale);
      }
      if (this._currentOption.slotLabelFormat !== options.slotLabelFormat) {
        this._calendarRef.fullCalendar('option', 'slotLabelFormat', options.slotLabelFormat);
      }
      if (this._currentOption.minTime !== options.minTime) {
        this._calendarRef.fullCalendar('option', 'minTime', options.minTime);
      }
      if (this._currentOption.maxTime !== options.maxTime) {
        this._calendarRef.fullCalendar('option', 'maxTime', options.maxTime);
      }
      if (this._currentOption.businessHours !== options.businessHours) {
        this._calendarRef.fullCalendar('option', 'businessHours', options.businessHours);
      }
      if (this._currentOption.defaultView !== options.defaultView) {
        this._calendarRef.fullCalendar('changeView', options.defaultView);
      }
      if (this._currentOption.weekends !== options.weekends) {
        // TODO destroy and re-create the calendar
        this._calendarRef.fullCalendar('option', 'weekends', options.weekends);
      }
      if (this._currentOption.header !== options.header) {
        this._calendarRef.fullCalendar('option', 'header', options.header);
      }
      // if (this._currentOption.buttonText !== options.buttonText) {
      //   this._calendarRef.fullCalendar('option', 'buttonText', options.buttonText);
      // }
      if (this._currentOption.columnHeaderFormat !== options.columnHeaderFormat) {
        this._calendarRef.fullCalendar('option', 'columnHeaderFormat', options.columnHeaderFormat);
      }
      if (this._currentOption.eventOrder !== options.eventOrder) {
        this._calendarRef.fullCalendar('option', 'eventOrder', options.eventOrder);
      }
      if (this._currentOption.groupByResource !== options.groupByResource) {
        this._calendarRef.fullCalendar('option', 'groupByResource', options.groupByResource);
      }
      if (this._currentOption.allDaySlot !== options.allDaySlot) {
        this._calendarRef.fullCalendar('option', 'allDaySlot', options.allDaySlot);
      }
      if (this._currentOption.allDayText !== options.allDayText) {
        this._calendarRef.fullCalendar('option', 'allDayText', options.allDayText);
      }
      if (this._currentOption.slotEventOverlap !== options.slotEventOverlap) {
        this._calendarRef.fullCalendar('option', 'slotEventOverlap', options.slotEventOverlap);
      }
    }
    this._currentOption = options;
  }

  private refreshResources(resources: ResourceInput[]) {
    this._resources = resources;
    if (typeof this._calendarRef !== 'undefined' && this._resources.length) {
      this._calendarRef.fullCalendar('refetchResources');
    }
  }

  private refreshDatePosition(datePosition: string) {
    if (typeof this._calendarRef !== 'undefined' && datePosition) {
      const currentDate = this._calendarRef.fullCalendar('getDate').format('DD-MM-YYYY');
      if (currentDate !== datePosition) {
        this._calendarRef.fullCalendar('gotoDate', datePosition);
      }
    }
    this._datePosition = datePosition;
  }

  private refreshEvents(events: Array<EventObjectInput>) {
    this._events = events;
    if (typeof this._calendarRef !== 'undefined') {
      this._calendarRef.fullCalendar('refetchEvents');
    }

    // if (this._events.length !== events.length) {
    //   this._events = events;
    //   if (typeof this._calendarRef !== 'undefined') {
    //     this._calendarRef.fullCalendar('refetchEvents');
    //   }
    // } else {
    //   const missing = differenceBy(events, this._events, 'id');
    //   const added = differenceBy(this._events, events, 'id');
    //   if (missing.length || added.length) {
    //     this._events = events;
    //     if (typeof this._calendarRef !== 'undefined') {
    //       this._calendarRef.fullCalendar('refetchEvents');
    //     }
    //   } else {
    //     const toUpdate = differenceWith(events, this._events, isEqual);
    //     if (toUpdate.length) {
    //       this._calendarRef.fullCalendar('updateEvents', events);
    //       this._calendarRef.fullCalendar('next');
    //       this._calendarRef.fullCalendar('prev');
    //       console.log('updateEvents');
    //     }
    //     this._events = events;
    //     this._calendarRef.fullCalendar('refetchEvents');
    //   }
    // }
  }
}
