export enum CalendarView {
  basic,
  agendaWeek,
  agendaDay,
  month,
  listWeek
}
