import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridComponent } from './components/grid/grid.component';
import { AgGridModule } from 'ag-grid-angular';
import { MaterialImportModule } from '../material-import/material-import.module';
import { FormsModule } from '@angular/forms';
import { GridActionsComponent } from './components/grid-actions/grid-actions.component';
import { IconsModule } from '../icons/icons.module';
import { GridItemIscheckedComponent } from './components/grid-item-ischecked/grid-item-ischecked.component';

const agComponent = [GridActionsComponent, GridItemIscheckedComponent];

@NgModule({
  imports: [
    CommonModule,
    MaterialImportModule,
    FormsModule,
    IconsModule,
    AgGridModule.withComponents(agComponent)
  ],
  declarations: [GridComponent, GridActionsComponent, GridItemIscheckedComponent],
  exports: [AgGridModule, GridComponent, GridItemIscheckedComponent]
})
export class GridModule {}
