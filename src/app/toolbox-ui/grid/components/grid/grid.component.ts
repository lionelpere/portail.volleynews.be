import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GridSearch } from '../../models/grid-search';
import { GridOptions, GridReadyEvent, ColDef, ColGroupDef, RowNode } from 'ag-grid';
import { GridAction } from '../../enums/grid-action.enum';
import { GridActionsDef } from '../../models/grid-actions-def';
import { GridActionsComponent } from '../grid-actions/grid-actions.component';
import { GridActionEvent } from '../../models/grid-action-event';
import { GridFilter } from '../../models/grid-filter';
import { HEX } from '../../../color';

const DefaultGridOptions: GridOptions = {
  enableFilter: true,
  // floatingFilter: true,
  // showToolPanel: true,
  enableSorting: true,
  enableColResize: true,
  paginationAutoPageSize: false,
  rowSelection: 'single',
  suppressCsvExport: false
};

const DefaultGridActions: GridActionsDef = {
  field: undefined
};

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {
  public searchObject: GridSearch;
  private _autofitColumns: boolean = true;
  private _noFilter: boolean = false;
  private _pagination: boolean = false;

  @Output('gridReady')
  gridReady: EventEmitter<any> = new EventEmitter<any>();
  @Output('gridAction')
  gridAction: EventEmitter<GridActionEvent> = new EventEmitter<GridActionEvent>();

  private _gridColumnDefs: (ColDef | ColGroupDef)[] = [];
  public icons;
  @Input('gridColumnDefs')
  public set gridColumnDefs(gridColumnDefs: (ColDef | ColGroupDef)[]) {
    if (gridColumnDefs) {
      this._gridColumnDefs = this.addActionColumn(gridColumnDefs);
    }

    if (this.gridOptions.api) {
      this.gridOptions.api.setColumnDefs(this._gridColumnDefs);
      if (this._autofitColumns) {
        this.gridOptions.api.sizeColumnsToFit();
      }
    }
  }

  @Input('pagination')
  public set pagination(pagination: boolean) {
    this._pagination = pagination;
  }

  public get pagination(): boolean {
    return this._pagination;
  }

  @Input('noFilter')
  public set noFilter(noFilter: boolean) {
    this._noFilter = noFilter;
  }

  public get noFilter(): boolean {
    return this._noFilter;
  }

  @Input('autofitColumns')
  public set autofitColumns(autofitColumns: boolean) {
    this._autofitColumns = autofitColumns;
  }

  public get gridColumnDefs() {
    return this._gridColumnDefs;
  }

  private _gridOptions: GridOptions;
  @Input('gridOptions')
  public set gridOptions(gridOptions: GridOptions) {
    const colDef = gridOptions.columnDefs;
    gridOptions.columnDefs = [];

    this._gridOptions = { ...this._gridOptions, ...gridOptions };

    if (
      colDef &&
      colDef.length > 0 &&
      (this._gridOptions.columnDefs.length === 0 ||
        (this.gridActionsDef && this._gridOptions.columnDefs.length === 1))
    ) {
      this.gridColumnDefs = colDef;
    }
  }
  public get gridOptions() {
    return this._gridOptions;
  }

  private _gridActionsDef: GridActionsDef;
  @Input('gridActionsDef')
  public set gridActionsDef(gridActions: GridActionsDef) {
    this._gridActionsDef = gridActions;

    if (gridActions.field) {
      this.gridColumnDefs = this._gridColumnDefs;
    }
  }
  public get gridActionsDef(): GridActionsDef {
    return this._gridActionsDef;
  }
  private _gridFilters: Array<GridFilter>;
  @Input('gridFilters')
  public set gridFilters(gridFilters: Array<GridFilter>) {
    this._gridFilters = gridFilters.map(f => ({
      ...f,
      isActive: typeof f.isActive !== 'undefined' ? f.isActive : true
    }));
  }
  public get gridFilters(): Array<GridFilter> {
    return this._gridFilters;
  }

  private _gridData: any[];
  @Input('gridData')
  public set girdData(data: any[]) {
    // if (data) {
    this._gridData = [...data];
    if (this.gridOptions.api) {
      if (!this.gridOptions.columnDefs || this.gridOptions.columnDefs.length === 0) {
        this.gridColumnDefs = this._gridColumnDefs;
      }
      this.gridOptions.api.setRowData(this._gridData);
    }
    // }
  }
  public get girdData(): any[] {
    return [...this._gridData];
  }
  @Input()
  quickSearchLabel: string;

  constructor() {
    this.searchObject = { quickSearch: '' };
    this.quickSearchLabel = 'Search';
    this.gridActionsDef = { ...DefaultGridActions };
    this._gridOptions = <GridOptions>{
      ...DefaultGridOptions,
      context: {
        componentParent: this
      },
      isExternalFilterPresent: () => !!this.gridFilters && this.gridFilters.length > 0,
      doesExternalFilterPass: this.doesExternalFilterPass.bind(this)
    };

    this.icons = {
      filter: ' ',
      menu: ' ',
      columns: ' ',
      sortAscending: ' ',
      sortDescending: ' ',
      sortUnSort: ' '
    };
  }

  ngOnInit() {}

  private addActionColumn(columns: (ColDef | ColGroupDef)[]): (ColDef | ColGroupDef)[] {
    const actionCol = columns.find(c => c.headerName === '#');
    if (!!actionCol || typeof this.gridActionsDef === 'undefined' || this.gridActionsDef === null) {
      return columns;
    }

    if (
      !this.gridActionsDef.showInfo &&
      !this.gridActionsDef.showEdit &&
      !this.gridActionsDef.showDelete
    ) {
      return columns;
    }

    return [
      ...columns.reverse(),
      <ColDef>{
        field: this.gridActionsDef.field,
        headerName: '#',
        width:
          (this.gridActionsDef.showInfo ? 50 : 0) +
          (this.gridActionsDef.showEdit ? 50 : 0) +
          (this.gridActionsDef.showDelete ? 50 : 0),
        cellStyle: { padding: 0 },
        suppressFilter: true,
        suppressMenu: true,
        suppressSorting: true,
        suppressMovable: true,
        suppressResize: true,
        suppressSizeToFit: true,
        cellRendererFramework: GridActionsComponent
      }
    ].reverse();
  }

  onFilterTextBoxChanged() {
    this._gridOptions.api.setQuickFilter(this.searchObject.quickSearch);
  }

  onExternalFilterChanged() {
    this._gridOptions.api.onFilterChanged();
  }

  onGridReady(event: GridReadyEvent) {
    if (
      !this.gridOptions.columnDefs ||
      this.gridOptions.columnDefs.length === 0 ||
      (this.gridActionsDef && this.gridOptions.columnDefs.length === 1)
    ) {
      this.gridColumnDefs = this._gridColumnDefs;
    }
    event.api.setRowData(this._gridData);
    if (!!this.gridFilters && this.gridFilters.length > 0) {
      event.api.onFilterChanged();
    }

    this.gridReady.emit(event);
  }

  private doesExternalFilterPass(node: RowNode): boolean {
    for (const filter of this.gridFilters) {
      const fct = filter.func;
      if (fct) {
        const filterResult = fct(node.data);
        if (filterResult === true && filter.isActive === false) {
          return false;
        }
      }
    }
    return true;
  }

  public getBoxShadowColor(hexColor: string): string {
    return `0 3px 1px -2px ${new HEX(hexColor)
      .toRGB()
      .darken(5)
      .setAlpha(0.2)
      .toString()},
    0 2px 2px 0 ${new HEX(hexColor)
      .toRGB()
      .darken(5)
      .setAlpha(0.14)
      .toString()},
    0 1px 5px 0 ${new HEX(hexColor)
      .toRGB()
      .setAlpha(0.12)
      .toString()}`;
  }

  sendGridAction(gridAction: GridAction, rowData: any) {
    this.gridAction.emit({ gridAction, rowData });
  }
}
