import { Component, OnInit } from '@angular/core';
import {IAfterGuiAttachedParams} from 'ag-grid';
import {ICellRendererAngularComp} from 'ag-grid-angular';

@Component({
  selector: 'app-grid-item-ischecked',
  templateUrl: './grid-item-ischecked.component.html',
  styleUrls: ['./grid-item-ischecked.component.scss']
})
export class GridItemIscheckedComponent implements OnInit, ICellRendererAngularComp {
  public params: any;
  public isChecked: boolean;

  private get componentParent(): any {
    if (this.params) {
      if (this.params.context) {
        if (this.params.context.componentParent) {
          return this.params.context.componentParent;
        }
      }
    }
    return null;
  }

  constructor() {}

  ngOnInit() {}

  agInit(params: any): void {
    this.params = params;
    this.isChecked = this.params.value;

  }

  refresh(params: any): boolean {
    this.params = params;
    this.isChecked = this.params.value;
    return true;
  }
  afterGuiAttached?(params?: IAfterGuiAttachedParams): void {}
}
