import { Component, OnInit } from '@angular/core';
import { GridAction } from '../../enums/grid-action.enum';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IAfterGuiAttachedParams } from 'ag-grid';
import { GridActionsDef } from '../../models/grid-actions-def';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-grid-actions',
  templateUrl: './grid-actions.component.html',
  styleUrls: ['./grid-actions.component.scss']
})
export class GridActionsComponent implements OnInit, ICellRendererAngularComp {
  public params: any;

  public get gridActionsDef(): GridActionsDef {
    if (this.componentParent) {
      return this.componentParent.gridActionsDef;
    }
    return null;
  }

  private get componentParent(): any {
    if (this.params) {
      if (this.params.context) {
        if (this.params.context.componentParent) {
          return this.params.context.componentParent;
        }
      }
    }
    return null;
  }

  constructor() {}

  ngOnInit() {
  }

  agInit(params: any): void {
    this.params = params;
  }

  refresh(params: any): boolean {
    this.params = params;
    return true;
  }
  afterGuiAttached?(params?: IAfterGuiAttachedParams): void {}

  info() {
    if (
      this.componentParent &&
      typeof this.componentParent.sendGridAction === 'function'
    ) {
      this.componentParent.sendGridAction(GridAction.INFO, this.params.data);
    }
  }

  edit() {
    if (
      this.componentParent &&
      typeof this.componentParent.sendGridAction === 'function'
    ) {
      this.componentParent.sendGridAction(GridAction.EDIT, this.params.data);
    }
  }

  delete() {
    if (
      this.componentParent &&
      typeof this.componentParent.sendGridAction === 'function'
    ) {
      this.componentParent.sendGridAction(GridAction.DELETE, this.params.data);
    }
  }
}
