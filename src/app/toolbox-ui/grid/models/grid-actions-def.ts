export interface GridActionsDef {
  field?: string;
  showInfo?: boolean;
  showEdit?: boolean;
  showDelete?: boolean;
}
