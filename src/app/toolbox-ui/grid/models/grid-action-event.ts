import { GridAction } from '../enums/grid-action.enum';

export interface GridActionEvent {
  gridAction: GridAction;
  rowData: any;
}
