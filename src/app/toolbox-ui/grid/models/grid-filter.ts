export interface GridFilter {
  field: string;
  label: string;
  filterColor?: string;
  isActive?: boolean;
  func: (Data: any) => boolean;
}
