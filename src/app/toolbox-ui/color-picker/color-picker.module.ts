import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ColorPickerModule as NgxColorPickerModule } from 'ngx-color-picker';

@NgModule({
  imports: [CommonModule, NgxColorPickerModule],
  declarations: [],
  exports: [NgxColorPickerModule]
})
export class ColorPickerModule {}
