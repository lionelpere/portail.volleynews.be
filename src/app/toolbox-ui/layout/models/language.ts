export interface Language {
  Key: string;
  Alt: string;
  Title: string;
}
