import { QueryParamsHandling } from '@angular/router/src/config';

export interface Breadcrumb {
  id?: number;
  title?: string;
  unTranslatedTitle?: string;
  route?: string;
  queryParamsHandling?: QueryParamsHandling | null;
  active?: boolean;
}
