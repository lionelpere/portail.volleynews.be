export interface LayoutValue {
  menuCollapsed: boolean;
  menuMinified: boolean;
  fullscreenToggled: boolean;
  mobileViewActivated: boolean;
  widgetFullscreen: boolean;
}
