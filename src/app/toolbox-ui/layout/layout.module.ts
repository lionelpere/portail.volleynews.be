import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterBarComponent } from './components/footer-bar/footer-bar.component';
import { MainContentWidgetFooterComponent } from './components/main-content-widget-footer/main-content-widget-footer.component';
import { MainContentWidgetBodyComponent } from './components/main-content-widget-body/main-content-widget-body.component';
import { MainContentWidgetHeaderComponent } from './components/main-content-widget-header/main-content-widget-header.component';
import { MainContentRibbonComponent } from './components/main-content-ribbon/main-content-ribbon.component';
import { MainContentContainerComponent } from './components/main-content-container/main-content-container.component';
import { MainContentWidgetComponent } from './components/main-content-widget/main-content-widget.component';
import { MainContentComponent } from './components/main-content/main-content.component';
import { NavSideMinifyMenuComponent } from './components/nav-side-minify-menu/nav-side-minify-menu.component';
import { NavSideMenuComponent } from './components/nav-side-menu/nav-side-menu.component';
import { NavSideLoginInfoComponent } from './components/nav-side-login-info/nav-side-login-info.component';
import { NavSideMenuItemComponent } from './components/nav-side-menu-item/nav-side-menu-item.component';
import { NavSideMenuGroupItemComponent } from './components/nav-side-menu-group-item/nav-side-menu-group-item.component';
import { NavSideBarComponent } from './components/nav-side-bar/nav-side-bar.component';
import { NavLogoutComponent } from './components/nav-logout/nav-logout.component';
import { NavFullScreenComponent } from './components/nav-full-screen/nav-full-screen.component';
import { NavLanguageDropdownComponent } from './components/nav-language-dropdown/nav-language-dropdown.component';
import { NavCollapseMenuComponent } from './components/nav-collapse-menu/nav-collapse-menu.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { LayoutService } from './services/layout.service';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { NgxBootstrapImportModule } from '../ngx-bootstrap-import/ngx-bootstrap-import.module';
import { IconsModule } from '../icons/icons.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, RouterModule, IconsModule, NgxBootstrapImportModule],
  declarations: [
    NavBarComponent,
    NavCollapseMenuComponent,
    NavLanguageDropdownComponent,
    NavFullScreenComponent,
    NavLogoutComponent,
    NavSideBarComponent,
    NavSideMenuGroupItemComponent,
    NavSideMenuItemComponent,
    NavSideLoginInfoComponent,
    NavSideMenuComponent,
    NavSideMinifyMenuComponent,
    MainContentComponent,
    MainContentWidgetComponent,
    MainContentContainerComponent,
    MainContentRibbonComponent,
    MainContentWidgetHeaderComponent,
    MainContentWidgetBodyComponent,
    MainContentWidgetFooterComponent,
    FooterBarComponent,
    BreadcrumbsComponent
  ],
  providers: [LayoutService],
  exports: [
    NavBarComponent,
    NavCollapseMenuComponent,
    NavLanguageDropdownComponent,
    NavFullScreenComponent,
    NavLogoutComponent,
    NavSideBarComponent,
    NavSideMenuGroupItemComponent,
    NavSideMenuItemComponent,
    NavSideLoginInfoComponent,
    NavSideMenuComponent,
    NavSideMinifyMenuComponent,
    MainContentComponent,
    MainContentWidgetComponent,
    MainContentContainerComponent,
    MainContentRibbonComponent,
    MainContentWidgetHeaderComponent,
    MainContentWidgetBodyComponent,
    MainContentWidgetFooterComponent,
    FooterBarComponent,
    BreadcrumbsComponent
  ]
})
export class LayoutModule {}
