import { Component, OnInit, Input } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-nav-side-login-info',
  templateUrl: './nav-side-login-info.component.html',
  styleUrls: ['./nav-side-login-info.component.scss']
})
export class NavSideLoginInfoComponent implements OnInit {
  @Input() username: string;
  constructor() { }

  ngOnInit() {
  }

}
