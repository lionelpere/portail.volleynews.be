import {
  Component,
  OnInit,
  Input,
  ElementRef,
  OnDestroy,
  AfterContentChecked
} from '@angular/core';
import { LayoutService } from '../../services/layout.service';
import { Subscription } from 'rxjs';



import { LayoutValue } from '../../models/layout-value';
import { Router, NavigationEnd } from '@angular/router';
import { tap } from 'rxjs/operators';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-nav-side-menu-group-item',
  templateUrl: './nav-side-menu-group-item.component.html',
  styleUrls: ['./nav-side-menu-group-item.component.scss']
})
export class NavSideMenuGroupItemComponent
  implements OnInit, OnDestroy, AfterContentChecked {
  @Input() iconSource: string;
  @Input() icon: string;
  @Input() title: string;
  @Input() iconColor: string;

  private _subLayoutService: Subscription;
  private _layoutStore: LayoutValue;
  private _activeLinkFound: boolean;

  private _isOpen: boolean;
  public get isOpen() {
    return this._isOpen && !this._layoutStore.menuMinified;
  }

  constructor(
    private _elem: ElementRef,
    private _layoutService: LayoutService
  ) {
    this._isOpen = false;
    this._activeLinkFound = false;
  }

  ngOnInit(): void {
    this._subLayoutService = this._layoutService.layoutStore
      .pipe(tap(store => (this._layoutStore = store)))
      .subscribe();
  }

  ngOnDestroy(): void {
    this._subLayoutService.unsubscribe();
  }

  ngAfterContentChecked(): void {
    if (this._activeLinkFound === false) {
      const elems = document.querySelectorAll('ui-nav-side-menu-item.active');
      const activeElem = this._elem.nativeElement.querySelectorAll('.active');
      this._isOpen = this._isOpen || activeElem.length > 0;
      this._activeLinkFound = elems.length > 0 || this._isOpen;
    }
  }

  toggle() {
    if (this._layoutStore.menuMinified === false) {
      this._isOpen = !this.isOpen;
    } else {
      this._isOpen = false;
    }
  }
}
