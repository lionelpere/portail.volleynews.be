import { Component, OnInit, Input } from '@angular/core';
import { LayoutService } from '../../services/layout.service';
import { WidgetService } from '../../services/widget.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-main-content-widget-header',
  templateUrl: './main-content-widget-header.component.html',
  styleUrls: ['./main-content-widget-header.component.scss']
})
export class MainContentWidgetHeaderComponent implements OnInit {
  @Input() title: string;
  @Input() iconSource: 'fas' | 'far' | 'fab';
  @Input() iconSize: 'lg' | 'xs' | '1x';
  @Input() iconColor: string;
  @Input() iconCounter: string;
  @Input() iconSpin: boolean;
  @Input() iconPulse: boolean;
  @Input() icon: string;
  @Input() fullscreenBtn: boolean;
  @Input() toggleBtn: boolean;

  public isFullScreen: boolean;
  public isToggle: boolean;

  constructor(private _widgetService: WidgetService) {
    this.isFullScreen = false;
    this.iconSource = 'fas';
    this.iconColor = '#FFF';
    this.iconCounter = '';
    this.iconSize = '1x';
  }

  ngOnInit() {}

  fullscreen() {
    this.isFullScreen = !this.isFullScreen;
    this._widgetService.fullscreen(this.isFullScreen);
  }

  toggle() {
    this.isToggle = !this.isToggle;
    this._widgetService.toggle(this.isToggle);
  }
}
