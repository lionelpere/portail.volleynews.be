import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  Renderer2,
  OnDestroy
} from '@angular/core';
import { WidgetService } from '../../services/widget.service';
import { Subscription, Observable, fromEvent, merge } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-main-content-widget-body',
  templateUrl: './main-content-widget-body.component.html',
  styleUrls: ['./main-content-widget-body.component.scss']
})
export class MainContentWidgetBodyComponent implements OnInit, OnDestroy {
  @ViewChild('widgetDiv') widgetDiv: ElementRef;
  @Input() noPadding: string;
  private _subWidgetService: Subscription;
  private _subWindowSize: Subscription;

  constructor(private _widgetService: WidgetService, private _renderer: Renderer2) {}
  ngOnInit() {
    const $1 = this._widgetService.$fullscreen.pipe(tap(fs => this.fullscreen(fs)));
    this._subWidgetService = merge($1).subscribe();
  }

  ngOnDestroy(): void {
    this._subWidgetService.unsubscribe();
    if (typeof this._subWindowSize !== 'undefined') {
      this._subWindowSize.unsubscribe();
    }
  }

  private fullscreen(fullscreen: boolean): void {
    if (fullscreen) {
      this.setBodySize();
      this._subWindowSize = fromEvent(window, 'resize')
        .pipe(debounceTime(10), tap(() => this.setBodySize()))
        .subscribe();
    } else if (typeof this._subWindowSize !== 'undefined') {
      this._subWindowSize.unsubscribe();
      this.resetBodySize();
    }
  }

  private setBodySize(): void {
    this._renderer.setStyle(this.widgetDiv.nativeElement, 'height', `${window.innerHeight - 34}px`);
  }

  private resetBodySize(): void {
    this._renderer.removeStyle(this.widgetDiv.nativeElement, 'height');
  }
}
