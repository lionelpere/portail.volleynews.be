import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../../services/layout.service';
import { Subscription } from 'rxjs';
import { LayoutValue } from '../../models/layout-value';
import { tap } from 'rxjs/operators';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-nav-side-minify-menu',
  templateUrl: './nav-side-minify-menu.component.html',
  styleUrls: ['./nav-side-minify-menu.component.scss']
})
export class NavSideMinifyMenuComponent implements OnInit {
  private _subLayoutService: Subscription;
  private _layoutStore: LayoutValue;

  public get minified(): boolean {
    return this._layoutStore.menuMinified;
  }

  constructor(private _layoutService: LayoutService) {}

  ngOnInit() {
    this._subLayoutService = this._layoutService.layoutStore
      .pipe(tap(store => (this._layoutStore = store)))
      .subscribe();
  }

  toggle() {
    this._layoutService.onMinifyMenu();
  }
}
