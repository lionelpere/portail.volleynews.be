import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { WidgetService } from '../../services/widget.service';
import { Subscription, Observable, merge } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-main-content-widget',
  templateUrl: './main-content-widget.component.html',
  styleUrls: ['./main-content-widget.component.scss'],
  providers: [WidgetService]
})
export class MainContentWidgetComponent implements OnInit, OnDestroy {
  @ViewChild('widget') widget: ElementRef;
  private _subWidgetService: Subscription;

  public isToggle: boolean;

  constructor(private _widgetService: WidgetService, private _renderer: Renderer2) {
    this.isToggle = false;
  }

  ngOnInit() {
    const $1 = this._widgetService.$fullscreen.pipe(tap(fs => this.processFullscreen(fs)));
    const $2 = this._widgetService.$toggle.pipe(tap(t => (this.isToggle = t)));
    this._subWidgetService = merge($1, $2).subscribe();
  }

  ngOnDestroy(): void {
    this._subWidgetService.unsubscribe();
  }

  private processFullscreen(isFullscreen: boolean) {
    if (isFullscreen === true) {
      this._renderer.setAttribute(this.widget.nativeElement, 'id', 'jarviswidget-fullscreen-mode');
    } else {
      this._renderer.removeAttribute(this.widget.nativeElement, 'id');
    }
  }
}
