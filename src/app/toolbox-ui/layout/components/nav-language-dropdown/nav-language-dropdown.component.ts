import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Input } from '@angular/core';
import { Language } from '../../models/language';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-nav-language-dropdown',
  templateUrl: './nav-language-dropdown.component.html',
  styleUrls: ['./nav-language-dropdown.component.scss']
})
export class NavLanguageDropdownComponent implements OnInit {
  @Output()
  public LanguageSelected: EventEmitter<Language> = new EventEmitter<Language>();
  @Input() public languages: Array<Language>;
  @Input() public currentLanguage: Language;

  public getFlagClass(language: Language): string {
    return language && `flag-${language.Key.toLocaleLowerCase()}`;
  }

  constructor() {}

  ngOnInit() {}

  setLanguage(language: Language) {
    this.LanguageSelected.emit(language);
  }
}
