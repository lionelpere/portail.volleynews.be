import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../../services/layout.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-collapse-menu',
  templateUrl: './nav-collapse-menu.component.html',
  styleUrls: ['./nav-collapse-menu.component.scss']
})
export class NavCollapseMenuComponent implements OnInit {

  constructor(private layoutService: LayoutService) { }

  ngOnInit() {
  }

  onToggle() {
    this.layoutService.onCollapseMenu();
  }

}
