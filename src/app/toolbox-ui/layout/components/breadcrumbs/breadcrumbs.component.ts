import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Breadcrumb } from '../../models/breadcrumb';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {
  @Input() manualMode: boolean;
  @Input() breadcrumps: Array<Breadcrumb>;
  @Output('itemClicked') itemClicked: EventEmitter<Breadcrumb> = new EventEmitter<Breadcrumb>();

  constructor(private _router: Router) {
    this.manualMode = false;
  }

  itemClick(breadcrumb: Breadcrumb) {
    if (this.manualMode === false) {
      this._router.navigate([breadcrumb.route], {
        queryParamsHandling: !!breadcrumb.queryParamsHandling
          ? breadcrumb.queryParamsHandling
          : null
      });
    }
    this.itemClicked.emit(breadcrumb);
  }

  ngOnInit() {}
}
