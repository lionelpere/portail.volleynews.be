import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'ui-logout',
  templateUrl: './nav-logout.component.html',
  styleUrls: ['./nav-logout.component.scss']
})
export class NavLogoutComponent implements OnInit {
  @Output() LogoutClicked: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  logout() {
    this.LogoutClicked.emit();
  }
}
