import { Component, OnInit, Input, EventEmitter, OnDestroy, Host, Optional } from '@angular/core';
import { Output } from '@angular/core/src/metadata/directives';
import { LayoutValue } from '../../models/layout-value';
import { Subscription } from 'rxjs';
import { LayoutService } from '../../services/layout.service';
import { NavSideMenuGroupItemComponent } from '../nav-side-menu-group-item/nav-side-menu-group-item.component';
import { tap } from 'rxjs/operators';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-nav-side-menu-item',
  templateUrl: './nav-side-menu-item.component.html',
  styleUrls: ['./nav-side-menu-item.component.scss']
})
export class NavSideMenuItemComponent implements OnInit, OnDestroy {
  @Input()
  iconSource: 'fas' | 'far' | 'fab';
  @Input()
  iconSize: 'lg' | 'xs' | '1x';
  @Input()
  iconColor: string;
  @Input()
  iconCounter: string;
  @Input()
  icon: string;
  @Input()
  iconSpin: boolean;
  @Input()
  iconPulse: boolean;
  @Input()
  title: string;
  @Input()
  queryParamsHandling: string;

  private _links: string[];
  @Input()
  public set routerLink(links: string[]) {
    if (!!links && links.length > 0) {
      this._links = links;
    }
  }
  public get routerLink(): string[] {
    return this._links;
  }

  public get link(): string {
    return !!this._links && this._links.length > 0 ? this._links[0] : '';
  }

  private _subLayoutService: Subscription;
  private _layoutStore: LayoutValue;

  public get minified(): boolean {
    return this._layoutStore.menuMinified;
  }

  constructor(
    private _layoutService: LayoutService,
    @Optional()
    @Host()
    private _host: NavSideMenuGroupItemComponent
  ) {
    this.iconSource = 'fas';
    this.iconColor = '#000';
    this.iconCounter = '';
    this.iconSize = _host === null ? 'lg' : '1x';
  }

  ngOnInit() {
    this._subLayoutService = this._layoutService.layoutStore
      .pipe(tap(store => (this._layoutStore = store)))
      .subscribe();
  }

  click() {
    if (this._layoutStore.mobileViewActivated) {
      this._layoutService.onCollapseMenu();
    }
  }

  ngOnDestroy(): void {
    this._subLayoutService.unsubscribe();
  }
}
