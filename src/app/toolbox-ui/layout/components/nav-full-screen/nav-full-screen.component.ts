import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../../services/layout.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-full-screen',
  templateUrl: './nav-full-screen.component.html',
  styleUrls: ['./nav-full-screen.component.scss']
})
export class NavFullScreenComponent implements OnInit {

  constructor(private layoutService: LayoutService) { }

  ngOnInit() {
  }

  onToggle() {
    this.layoutService.onToggleFullscreen();
  }
}
