import { Component, OnInit, Input } from '@angular/core';
import { LayoutService } from '../../services/layout.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-footer-bar',
  templateUrl: './footer-bar.component.html',
  styleUrls: ['./footer-bar.component.scss']
})
export class FooterBarComponent implements OnInit {
  @Input() FixedFooter: boolean;
  @Input() NoSideBar: boolean;
  public dtNow: Date = new Date();

  constructor(private layoutService: LayoutService) {
    this.FixedFooter = false;
    this.NoSideBar = false;
    setInterval(() => {
      this.dtNow = new Date();
    }, 1);
  }

  ngOnInit() { }
}
