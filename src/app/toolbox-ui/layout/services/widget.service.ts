import { Injectable } from '@angular/core';
import { BehaviorSubject ,  Observable } from 'rxjs';
import { LayoutService } from './layout.service';
import { distinctUntilChanged } from 'rxjs/operators';



@Injectable()
export class WidgetService {
  private _fullscreenSubject: BehaviorSubject<boolean>;
  private _toggleSubject: BehaviorSubject<boolean>;

  public get $fullscreen(): Observable<boolean> {
    return this._fullscreenSubject.asObservable().pipe(distinctUntilChanged());
  }

  public get $toggle(): Observable<boolean> {
    return this._toggleSubject.asObservable().pipe(distinctUntilChanged());
  }

  constructor(private _layoutService: LayoutService) {
    this._fullscreenSubject = new BehaviorSubject<boolean>(false);
    this._toggleSubject = new BehaviorSubject<boolean>(false);
  }

  fullscreen(isFullScreen: boolean) {
    this._layoutService.onWidgetFullScreen(isFullScreen);
    this._fullscreenSubject.next(isFullScreen);
  }

  toggle(isToggle: boolean) {
    this._toggleSubject.next(isToggle);
  }
}
