import { Injectable } from '@angular/core';
import { LayoutValue } from './../models/layout-value';
import { Observable ,  BehaviorSubject ,  Subscription, fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';

@Injectable()
export class LayoutService {
  private store: LayoutValue = {
    menuCollapsed: false,
    menuMinified: false,
    fullscreenToggled: false,
    mobileViewActivated: false,
    widgetFullscreen: false
  };
  private html: HTMLHtmlElement = document.getElementsByTagName('html')[0];
  private body: HTMLBodyElement = document.getElementsByTagName('body')[0];
  private subject: BehaviorSubject<LayoutValue> = new BehaviorSubject<
    LayoutValue
  >(this.store);

  private resizeSub: Subscription;

  public get layoutStore(): Observable<LayoutValue> {
    return this.subject.asObservable();
  }

  constructor() {
    this.body.classList.add('smart-style-6');
    this.resizeSub = fromEvent(window, 'resize').pipe(
      debounceTime(100),
      map(() => {
        this.processBody();
      }))
      .subscribe();
  }

  onWidgetFullScreen(value?: boolean) {
    if (typeof value !== 'undefined') {
      this.store.widgetFullscreen = value;
    } else {
      this.store.widgetFullscreen = !this.store.widgetFullscreen;
    }
    this.processBody();
  }

  onCollapseMenu(value?: boolean) {
    if (typeof value !== 'undefined') {
      this.store.menuCollapsed = value;
    } else {
      this.store.menuCollapsed = !this.store.menuCollapsed;
    }
    this.processBody();
  }

  onToggleFullscreen(value?: boolean) {
    const documentMethods = {
      enter: [
        'requestFullscreen',
        'mozRequestFullScreen',
        'webkitRequestFullscreen',
        'msRequestFullscreen'
      ],
      exit: [
        'cancelFullScreen',
        'mozCancelFullScreen',
        'webkitCancelFullScreen',
        'msCancelFullScreen'
      ]
    };

    if (typeof value !== 'undefined') {
      this.store.fullscreenToggled = value;
    } else {
      this.store.fullscreenToggled = !this.store.fullscreenToggled;
    }

    if (this.store.fullscreenToggled) {
      this.body.classList.add('full-screen');
      document.documentElement[
        documentMethods.enter.filter(method => {
          return document.documentElement[method];
        })[0]
      ]();
    } else {
      this.body.classList.remove('full-screen');
      document[
        documentMethods.exit.filter(method => {
          return document[method];
        })[0]
      ]();
    }
  }

  onMinifyMenu() {
    this.store.menuMinified = !this.store.menuMinified;
    this.processBody();
  }

  private processBody() {
    this.store.mobileViewActivated =
      (window.innerWidth || this.html.clientWidth) < 979;

    if (this.store.menuCollapsed) {
      this.body.classList.add('hidden-menu-mobile-lock');
      this.body.classList.add('hidden-menu');
    } else {
      this.body.classList.remove('hidden-menu-mobile-lock');
      this.body.classList.remove('hidden-menu');
    }

    if (this.store.menuMinified && !this.store.mobileViewActivated) {
      this.body.classList.add('minified');
      this.body.classList.remove('hidden-menu');
      this.body.classList.remove('hidden-menu-mobile-lock');
    } else {
      this.body.classList.remove('minified');
    }

    if (this.store.widgetFullscreen) {
      this.body.classList.add('nooverflow');
    } else {
      this.body.classList.remove('nooverflow');
    }

    this.subject.next(this.store);
  }
}
