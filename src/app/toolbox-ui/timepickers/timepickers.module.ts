import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimepickerModule } from 'ngx-bootstrap';
import { AmazingTimePickerModule } from 'amazing-time-picker'; // this line you need

@NgModule({
  imports: [CommonModule, TimepickerModule.forRoot(), AmazingTimePickerModule],
  declarations: [],
  exports: [TimepickerModule, AmazingTimePickerModule]
})
export class TimepickersModule {}
