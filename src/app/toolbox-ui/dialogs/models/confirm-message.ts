export interface ConfirmMessage {
  title: string;
  text: string;
  cancelBtn?: string;
  confirmBtn?: string;
  confirmColor?: 'primary' | 'warn' | 'accent';
}
