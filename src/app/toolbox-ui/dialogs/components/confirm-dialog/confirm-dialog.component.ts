import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfirmMessage } from '../../models/confirm-message';
import { ConfirmDialogResult } from '../../enums/confirm-dialog-result.enum';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmMessage
  ) {
    data.cancelBtn = data.cancelBtn || 'Cancel';
    data.confirmBtn = data.confirmBtn || 'Confirm';
    data.confirmColor = data.confirmColor || 'primary';
  }

  ngOnInit() {}

  cancel() {
    this.dialogRef.close(ConfirmDialogResult.Cancel);
  }

  confirm() {
    this.dialogRef.close(ConfirmDialogResult.Confirm);
  }
}
