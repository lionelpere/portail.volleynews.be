import { Component, OnInit, Renderer2, ElementRef, Input } from '@angular/core';
import { FormInputBase, MakeProvider } from '../form-input-base';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-form-input-text-inline',
  templateUrl: './form-input-text-inline.component.html',
  styleUrls: ['./form-input-text-inline.component.scss'],
  providers: [MakeProvider(FormInputTextInlineComponent)]
})
export class FormInputTextInlineComponent extends FormInputBase<string>
  implements OnInit {
  @Input() labelCol: number;
  @Input() inputCol: number;

  public get labelClass() {
    return `col-${this.labelCol}`;
  }
  public get inputClass() {
    return `col-${this.inputCol}`;
  }

  constructor(_renderer: Renderer2, _elem: ElementRef) {
    super(_renderer, _elem);
  }

  ngOnInit() {
    this.labelCol = 3;
    this.inputCol = 9;
  }

  Touched() {
    this.onTouched.forEach(_ => _());
  }

  change($event) {
    this.modelValue = $event.target.value;
  }
}
