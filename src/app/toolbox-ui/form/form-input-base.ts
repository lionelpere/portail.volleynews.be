import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Renderer2, ElementRef, forwardRef, Input } from '@angular/core';

export abstract class FormInputBase<T> implements ControlValueAccessor {
  @Input() label: string;
  @Input() helpMessage: string;
  @Input() placeholder: string;

  public inputId: string;

  private _modelValue: T;
  public get modelValue(): T {
    return this._modelValue;
  }
  public set modelValue(v: T) {
    if (v !== this._modelValue) {
      this._modelValue = v;
      this.onChanged.forEach(_ => _(v));
    }
  }

  constructor(private _renderer: Renderer2, private _elem: ElementRef) {
    this.inputId = `input_${this.generateUniqueId()}`;
    this.isDisabled = false;
    this.label = '';
    this.helpMessage = '';
    this.placeholder = '';
  }

  private onChanged: Array<(data: any) => void> = new Array<
    (value: any) => void
  >();
  public onTouched: Array<() => void> = new Array<() => void>();

  public isDisabled: boolean;

  public writeValue(value: any): void {
    const inputElem = this._elem.nativeElement.querySelectorAll('input')[0];
    this._modelValue = value;
    if (inputElem) {
      this._renderer.setAttribute(inputElem, 'value', value);
    }
  }
  public registerOnChange(fn: any): void {
    this.onChanged.push(fn);
  }
  public registerOnTouched(fn: any): void {
    this.onTouched.push(fn);
  }
  public setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  private generateUniqueId(): string {
    return (
      '_' +
      Math.random()
        .toString(36)
        .substr(2, 9)
    );
  }
}

export function MakeProvider(type: any) {
  return {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => type),
    multi: true
  };
}
