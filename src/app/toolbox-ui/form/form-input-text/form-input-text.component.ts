import {
  Component,
  OnInit,
  Input,
  AfterViewInit,
  forwardRef,
  ElementRef,
  ViewChild,
  Renderer2
} from '@angular/core';

import { FormInputBase, MakeProvider } from '../form-input-base';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-form-input-text',
  templateUrl: './form-input-text.component.html',
  styleUrls: ['./form-input-text.component.scss'],
  providers: [MakeProvider(FormInputTextComponent)]
})
export class FormInputTextComponent extends FormInputBase<string>
  implements OnInit {

  constructor(_renderer: Renderer2, _elem: ElementRef) {
    super(_renderer, _elem);
  }

  ngOnInit() {}

  Touched() {
    this.onTouched.forEach(_ => _());
  }

  change($event) {
    this.modelValue = $event.target.value;
  }
}
