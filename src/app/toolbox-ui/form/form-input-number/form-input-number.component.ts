import { Component, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { FormInputBase, MakeProvider } from '../form-input-base';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-form-input-number',
  templateUrl: './form-input-number.component.html',
  styleUrls: ['./form-input-number.component.scss'],
  providers: [MakeProvider(FormInputNumberComponent)]
})
export class FormInputNumberComponent extends FormInputBase<number>
implements OnInit {

constructor(_renderer: Renderer2, _elem: ElementRef) {
  super(_renderer, _elem);
}

ngOnInit() {}

Touched() {
  this.onTouched.forEach(_ => _());
}

change($event) {
  this.modelValue = $event.target.value;
}
}
