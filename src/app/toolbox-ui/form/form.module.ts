import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormInputTextComponent } from './form-input-text/form-input-text.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormSectionComponent } from './form-section/form-section.component';
import { FormInputTextInlineComponent } from './form-input-text-inline/form-input-text-inline.component';
import { FormInputNumberInlineComponent } from './form-input-number-inline/form-input-number-inline.component';
import { FormInputNumberComponent } from './form-input-number/form-input-number.component';
import { IconsModule } from '../icons/icons.module';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, IconsModule],
  declarations: [
    FormInputTextComponent,
    FormSectionComponent,
    FormInputTextInlineComponent,
    FormInputNumberInlineComponent,
    FormInputNumberComponent
  ],
  exports: [
    FormInputTextComponent,
    FormSectionComponent,
    FormInputTextInlineComponent,
    FormInputNumberComponent,
    FormInputNumberInlineComponent,
    ReactiveFormsModule
  ]
})
export class FormModule {}
