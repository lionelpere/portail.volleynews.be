import { Component, OnInit, Input } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {
  @Input() public loading: boolean;
  @Input() public width: string;
  @Input() public height: string;

  constructor() {
    this.loading = false;
   }

  ngOnInit() {
  }

}
