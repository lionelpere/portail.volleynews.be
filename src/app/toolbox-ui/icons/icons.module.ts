import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { IconComponent } from './icon/icon.component';

library.add(fas, far, fab);

@NgModule({
  imports: [CommonModule, FontAwesomeModule],
  exports: [FontAwesomeModule, IconComponent],
  declarations: [IconComponent]
})
export class IconsModule {
  // static forRoot(): ModuleWithProviders {
  //   library.add(fas, far, fab);
  //   return { ngModule: IconsModule };
  // }
}
