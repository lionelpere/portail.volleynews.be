import { Component, OnInit, Input, ElementRef, ViewChild, AfterViewInit } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss']
})
export class IconComponent implements OnInit {
  @Input() public source: 'fas' | 'far' | 'fab' | 'mat' | 'svg';
  @Input() public size: string;
  @Input() public spin: boolean;
  @Input() public pulse: boolean;
  @Input() public icon: string;
  @Input() public transform: '' | 'flip-v' | 'flip-h' | 'flip-v flip-h';
  @ViewChild('contentWrapper') content: ElementRef;

  public settingHtml: boolean;
  constructor() {
    this.settingHtml = false;
    this.source = 'fas';
    this.icon = '';
  }

  ngOnInit(): void {
    this.settingHtml = this.icon && this.icon.trim() !== '';
    const content = this.content;
    if (content) {
      const icon = content.nativeElement.innerHTML;
      if (icon) {
        this.icon = icon.trim();
      }
    }
  }
}
