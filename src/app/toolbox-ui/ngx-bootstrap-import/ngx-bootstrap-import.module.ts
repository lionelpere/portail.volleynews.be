import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsModule, BsDropdownModule } from 'ngx-bootstrap';

@NgModule({
  imports: [CommonModule, BsDropdownModule.forRoot(), TabsModule.forRoot()],
  declarations: [],
  exports: [BsDropdownModule, TabsModule]
})
export class NgxBootstrapImportModule {}
