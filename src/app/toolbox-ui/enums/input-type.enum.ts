export enum InputType {
  Text,
  Password,
  Number,
  Boolean
}
