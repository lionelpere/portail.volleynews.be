import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { LayoutModule } from './layout/layout.module';
import { LoginBoxModule } from './login-box/login-box.module';
import { WizardModule } from './wizard/wizard.module';
import { FormModule } from './form/form.module';
import { CalendarModule } from './calendar/calendar.module';
import { TimepickersModule } from './timepickers/timepickers.module';
import { ColorPickerModule } from './color-picker/color-picker.module';
import { NgxBootstrapImportModule } from './ngx-bootstrap-import/ngx-bootstrap-import.module';
import { IconsModule } from './icons/icons.module';
import { GridModule } from './grid/grid.module';
import { AgGridModule } from 'ag-grid-angular';
import { ButtonsModule } from './buttons/buttons.module';
import { DialogsModule } from './dialogs/dialogs.module';
import { MaterialImportModule } from './material-import/material-import.module';
import { LoadingModule } from './loading/loading.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    LayoutModule,
    LoginBoxModule,
    WizardModule,
    FormModule,
    CalendarModule,
    TimepickersModule,
    ColorPickerModule,
    NgxBootstrapImportModule,
    GridModule,
    AgGridModule,
    ButtonsModule,
    DialogsModule,
    FormsModule,
    MaterialImportModule,
    LoadingModule,
    IconsModule /*.forRoot()*/
  ],
  declarations: [],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    LoginBoxModule,
    WizardModule,
    FormModule,
    CalendarModule,
    TimepickersModule,
    ColorPickerModule,
    NgxBootstrapImportModule,
    IconsModule,
    GridModule,
    AgGridModule,
    DialogsModule,
    ButtonsModule,
    LoadingModule,
    MaterialImportModule
  ]
})
export class ToolboxUiModule {}
