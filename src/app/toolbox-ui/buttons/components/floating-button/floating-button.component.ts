import { Component, OnInit, Input } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-floating-button',
  templateUrl: './floating-button.component.html',
  styleUrls: ['./floating-button.component.scss']
})
export class FloatingButtonComponent implements OnInit {
  @Input() position: 'absolute' | 'fixed';
  @Input() size: 'small' | 'large';
  @Input() hAlign: 'left' | 'right';
  @Input() vAlign: 'top' | 'bottom';
  @Input() color: 'basic' | 'primary' | 'accent' | 'warn';
  @Input() iconSource: 'fas' | 'far' | 'fab';
  @Input() icon: string;
  @Input() disabled: boolean;
  @Input() title: string;

  public get buttonClass(): string {
    return `btn-${this.position} h-align-${this.hAlign} v-align-${this.vAlign}`;
  }

  constructor() {
    this.position = 'fixed';
    this.size = 'large';
    this.hAlign = 'right';
    this.vAlign = 'bottom';
    this.iconSource = 'fas';
    this.icon = 'plus';
    this.color = 'primary';
    this.disabled = false;
    this.title = 'button';
  }

  ngOnInit() {}
}
