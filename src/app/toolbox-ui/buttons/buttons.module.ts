import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FloatingButtonComponent } from './components/floating-button/floating-button.component';
import { MaterialImportModule } from '../material-import/material-import.module';
import { IconsModule } from '../icons/icons.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialImportModule,
    IconsModule
  ],
  declarations: [FloatingButtonComponent],
  exports: [FloatingButtonComponent]
})
export class ButtonsModule { }
