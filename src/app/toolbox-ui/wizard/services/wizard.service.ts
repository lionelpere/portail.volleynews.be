import { Injectable } from '@angular/core';
import { WizardStep } from '../models/wizard-step';
import { Observable, BehaviorSubject, Subject, combineLatest } from 'rxjs';

import { WizardNavClickEvent } from '../components/wizard-nav/wizard-nav-click-event.enum';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';

@Injectable()
export class WizardService {
  private readonly _defaultStepValue: WizardStep = {
    Id: Number.MIN_VALUE,
    Title: ''
  };
  private _steps: WizardStep[];
  private _currentStepId: number;
  private _$stepsSubject: BehaviorSubject<WizardStep[]>;

  private _$manualInitSubject: BehaviorSubject<boolean>;
  private _$manualModeSubject: BehaviorSubject<boolean>;
  private _$currentStepIdSubject: BehaviorSubject<number>;
  private _$clickEventsSubject: Subject<WizardNavClickEvent>;
  private _$stepSelectedSubject: Subject<WizardStep>;

  public setManualMode(manualEnable: boolean): void {
    this._$manualModeSubject.next(manualEnable);
  }

  public get $currentStep(): Observable<WizardStep> {
    return combineLatest(this._$currentStepIdSubject, this._$stepsSubject).pipe(
      map(s => s[1].find(st => st.Id === s[0])),
      filter(s => typeof s !== 'undefined'),
      distinctUntilChanged((s, ss) => s.Id === ss.Id)
    );
  }

  public setCurrentStep(stepId: number) {
    this._$currentStepIdSubject.next(stepId);
  }

  public get $lastStep(): Observable<WizardStep> {
    return this._$stepsSubject.pipe(
      map($s => $s.find(s => s.Id === Math.max(...this._steps.map(st => st.Id)))),
      filter(s => typeof s !== 'undefined'),
      distinctUntilChanged((s, ss) => s.Id === ss.Id)
    );
  }

  public get $firstStep(): Observable<WizardStep> {
    return this._$stepsSubject
      .asObservable()
      .pipe(
        map($s => $s.find(s => s.Id === Math.min(...this._steps.map(st => st.Id)))),
        filter(s => typeof s !== 'undefined'),
        distinctUntilChanged((s, ss) => s.Id === ss.Id)
      );
  }

  public get $steps(): Observable<WizardStep[]> {
    return this._$stepsSubject.asObservable();
  }

  public get $manualMode(): Observable<boolean> {
    return this._$manualModeSubject.asObservable().pipe(distinctUntilChanged());
  }

  public get $manualInit(): Observable<boolean> {
    return this._$manualInitSubject.asObservable().pipe(distinctUntilChanged());
  }

  public get $clickEvents(): Observable<WizardNavClickEvent> {
    return this._$clickEventsSubject.asObservable();
  }

  public get $stepSelected(): Observable<WizardStep> {
    return this._$stepSelectedSubject.asObservable();
  }

  constructor() {
    this._$currentStepIdSubject = new BehaviorSubject<number>(this._defaultStepValue.Id);
    this._steps = new Array<WizardStep>();
    this._$stepsSubject = new BehaviorSubject<WizardStep[]>(this._steps);
    this._$manualModeSubject = new BehaviorSubject<boolean>(false);
    this._$manualInitSubject = new BehaviorSubject<boolean>(false);
    this._$clickEventsSubject = new Subject<WizardNavClickEvent>();
    this._$stepSelectedSubject = new Subject<WizardStep>();
  }

  public registerSteps(steps: WizardStep[]) {
    this._steps = [...steps];
    this._$manualInitSubject.next(true);
    this._$stepsSubject.next(this._steps);
  }

  public registerStep(step: WizardStep) {
    if (this._$manualInitSubject.getValue() === false) {
      if (this._steps.some(s => s.Id === step.Id) === false) {
        this._steps = [...this._steps, step];
        this._$stepsSubject.next(this._steps);

        if (this._$manualModeSubject.getValue() === false) {
          const firstStep = this._steps.find(
            s => s.Id === Math.min(...this._steps.map(st => st.Id))
          );
          if (firstStep) {
            this._$currentStepIdSubject.next(firstStep.Id);
          }
        }
      }
    }
  }

  public selectStep(stepId: number): void {
    const stepSelected = this._steps.find(s => s.Id === stepId);
    if (stepSelected) {
      this._$stepSelectedSubject.next(stepSelected);
    }
  }

  public previousStep(): void {
    const _currentStepId = this._$currentStepIdSubject.getValue();
    const _previousStep = this._steps.find(s => s.Id === _currentStepId - 1);
    if (typeof _previousStep !== 'undefined') {
      if (this._$manualModeSubject.getValue() === false) {
        this._$currentStepIdSubject.next(_previousStep.Id);
      }
      this._$clickEventsSubject.next(WizardNavClickEvent.Back);
    }
  }
  public nextStep(): void {
    const _currentStepId = this._$currentStepIdSubject.getValue();
    const _nextStep = this._steps.find(s => s.Id === _currentStepId + 1);
    if (typeof _nextStep !== 'undefined') {
      if (this._$manualModeSubject.getValue() === false) {
        this._$currentStepIdSubject.next(_nextStep.Id);
      }
      this._$clickEventsSubject.next(WizardNavClickEvent.Next);
    } else {
      this._$clickEventsSubject.next(WizardNavClickEvent.Finish);
    }
  }
}
