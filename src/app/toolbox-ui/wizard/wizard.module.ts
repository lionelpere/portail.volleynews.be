import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WizardComponent } from './components/wizard/wizard.component';
import { WizardNavComponent } from './components/wizard-nav/wizard-nav.component';
import { WizardBodyComponent } from './components/wizard-body/wizard-body.component';
import { WizardNavStepComponent } from './components/wizard-nav-step/wizard-nav-step.component';
import { WizardBodyStepComponent } from './components/wizard-body-step/wizard-body-step.component';
import { IconsModule } from '../icons/icons.module';

@NgModule({
  imports: [CommonModule, IconsModule],
  declarations: [
    WizardComponent,
    WizardNavComponent,
    WizardBodyComponent,
    WizardNavStepComponent,
    WizardBodyStepComponent
  ],
  exports: [
    WizardComponent,
    WizardNavComponent,
    WizardBodyComponent,
    WizardNavStepComponent,
    WizardBodyStepComponent
  ]
})
export class WizardModule {}
