import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { WizardService } from '../../services/wizard.service';
import { WizardStep } from '../../models/wizard-step';
import { Subscription, Observable, merge } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-wizard-nav-step',
  templateUrl: './wizard-nav-step.component.html',
  styleUrls: ['./wizard-nav-step.component.scss']
})
export class WizardNavStepComponent implements OnInit, OnDestroy {
  @Input()
  public set title(title: string) {
    this._step.Title = title;
    this.titleToLong = this.getTextWidth(title) > 150;
  }
  @Input()
  public set stepId(stepId: number) {
    this._step.Id = stepId;
  }
  @Input() public isDisabled: boolean;

  private _step: WizardStep;
  private _subWizardService: Subscription;
  private _currentStep: WizardStep;
  private _manualInit: boolean;
  private _manualMode: boolean;
  private _canvas: HTMLCanvasElement;

  public get step(): WizardStep {
    return this._step;
  }

  public titleToLong: boolean;

  public get isCompleted(): boolean {
    return this._currentStep && this._step.Id < this._currentStep.Id;
  }

  public get isActive(): boolean {
    return this._currentStep && this._step.Id === this._currentStep.Id;
  }

  constructor(private _wizardService: WizardService) {
    this._step = { Id: 0, Title: '' };
  }

  click() {
    if (this.isCompleted && !this.isDisabled) {
      if (this._manualMode === false) {
        this._wizardService.setCurrentStep(this._step.Id);
      }
      this._wizardService.selectStep(this._step.Id);
    }
  }

  ngOnInit() {
    const $1 = this._wizardService.$currentStep.pipe(tap(s => (this._currentStep = s)));
    const $2 = this._wizardService.$manualInit.pipe(tap(i => (this._manualInit = i)));
    const $3 = this._wizardService.$manualMode.pipe(tap(i => (this._manualMode = i)));

    this._subWizardService = merge($1, $2, $3).subscribe();

    if (this._manualInit === false) {
      this._wizardService.registerStep(this._step);
    }
  }

  ngOnDestroy(): void {
    this._subWizardService.unsubscribe();
  }

  private getTextWidth(
    text: string,
    font: string = '16px "Open Sans", Arial, Helvetica, Sans-Serif'
  ): number {
    // re-use canvas object for better performance
    this._canvas = this._canvas || (this._canvas = document.createElement('canvas'));
    const context = this._canvas.getContext('2d');
    context.font = font;
    const metrics = context.measureText(text);
    return metrics.width;
  }
}
