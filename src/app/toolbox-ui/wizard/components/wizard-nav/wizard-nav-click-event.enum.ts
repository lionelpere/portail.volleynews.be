export enum WizardNavClickEvent {
  Next,
  Back,
  Finish
}
