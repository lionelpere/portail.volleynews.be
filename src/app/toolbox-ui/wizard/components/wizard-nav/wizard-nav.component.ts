import { Component, OnInit, OnDestroy, Input, ViewChild, ElementRef } from '@angular/core';
import { WizardService } from '../../services/wizard.service';
import { WizardStep } from '../../models/wizard-step';

import { Observable, Subscription, merge } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-wizard-nav',
  templateUrl: './wizard-nav.component.html',
  styleUrls: ['./wizard-nav.component.scss']
})
export class WizardNavComponent implements OnInit, OnDestroy {
  @ViewChild('stepContainer') stepContainer: ElementRef;
  @ViewChild('stepAction') stepAction: ElementRef;
  @Input() nextBtn: string;
  @Input() prevBtn: string;
  @Input() finishBtn: string;
  @Input() showNavBtn: boolean;

  private _manualInit: boolean;
  private _currentStep: WizardStep;
  private _firstStep: WizardStep;
  private _lastStep: WizardStep;
  private _steps: WizardStep[];

  private _subWizardService: Subscription;

  public marginLeft: number;

  public get manualInit(): boolean {
    return this._manualInit;
  }

  public get steps(): WizardStep[] {
    return [...this._steps];
  }

  public get isFirstStep(): boolean {
    return this._currentStep && this._firstStep && this._firstStep.Id === this._currentStep.Id;
  }

  public get isLastStep(): boolean {
    return this._currentStep && this._firstStep && this._lastStep.Id === this._currentStep.Id;
  }

  constructor(private _wizardService: WizardService) {
    this.marginLeft = 0;
    this._manualInit = false;
    this.showNavBtn = false;
    this.nextBtn = 'next';
    this.prevBtn = 'prev';
    this.finishBtn = 'finish';
  }

  ngOnInit() {
    const $1 = this._wizardService.$manualInit.pipe(tap(init => (this._manualInit = init)));
    const $2 = this._wizardService.$currentStep.pipe(
      tap(s => this.calculateMargin(s)),
      tap(s => (this._currentStep = s))
    );
    const $3 = this._wizardService.$firstStep.pipe(tap(s => (this._firstStep = s)));
    const $4 = this._wizardService.$lastStep.pipe(tap(s => (this._lastStep = s)));
    const $5 = this._wizardService.$steps.pipe(tap(s => (this._steps = [...s])));
    this._subWizardService = merge($1, $2, $3, $4, $5).subscribe();
  }

  ngOnDestroy(): void {
    this._subWizardService.unsubscribe();
  }

  prev(): void {
    this._wizardService.previousStep();
  }
  next(): void {
    this._wizardService.nextStep();
  }

  private calculateMargin(currentStep: WizardStep) {
    const eContainer = this.stepContainer.nativeElement;
    const eAction = this.stepAction && this.stepAction.nativeElement;
    const eActiveStep = eContainer.querySelectorAll(`#nav-step-${currentStep.Id}`)[0];

    if (!!eActiveStep) {
      const liSteps = [].slice.call(eContainer.querySelectorAll('li'));

      if (liSteps.length > 0) {
        let totalWidth: number;
        let containerWidth: number;

        totalWidth = liSteps.reduce((acc, e) => (acc += e.offsetWidth), 0);
        containerWidth = eContainer.clientWidth - (eAction ? eAction.offsetWidth : 0);
        if (totalWidth > containerWidth) {
          let newMargin = totalWidth - containerWidth;

          // set the position so that the active step is in a good
          // position if it has been moved out of view
          const offsetLeft = eActiveStep.offsetLeft - newMargin - this.marginLeft;
          if (offsetLeft < 100) {
            newMargin += offsetLeft - 100;
            if (newMargin < 1) {
              newMargin = 0;
            }
          }
          this.marginLeft = 0 - newMargin;
        } else {
          this.marginLeft = 0;
        }
      }
    }
  }
}
