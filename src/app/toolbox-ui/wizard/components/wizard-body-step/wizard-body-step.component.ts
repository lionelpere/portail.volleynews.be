import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { WizardService } from '../../services/wizard.service';
import { Subscription ,  Observable, merge } from 'rxjs';
import { WizardStep } from '../../models/wizard-step';
import { tap } from 'rxjs/operators';



@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-wizard-body-step',
  templateUrl: './wizard-body-step.component.html',
  styleUrls: ['./wizard-body-step.component.scss']
})
export class WizardBodyStepComponent implements OnInit, OnDestroy {
  @Input() stepId: number;

  private _currentStep: WizardStep;
  private _subWizardService: Subscription;

  public get isActive(): boolean {
    return this._currentStep && this.stepId === this._currentStep.Id;
  }

  constructor(private _wizardService: WizardService) {}

  ngOnInit() {
    const $1 = this._wizardService.$currentStep.pipe(tap(
      s => (this._currentStep = s)
    ));

    this._subWizardService = merge($1).subscribe();
  }
  ngOnDestroy(): void {
    this._subWizardService.unsubscribe();
  }
}
