import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { WizardNavClickEvent } from '../wizard-nav/wizard-nav-click-event.enum';
import { WizardService } from '../../services/wizard.service';
import { WizardStep } from '../../models/wizard-step';
import { Subscription ,  Observable, merge } from 'rxjs';
import { tap } from 'rxjs/operators';




@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
  providers: [WizardService]
})
export class WizardComponent implements OnInit, OnDestroy {
  @Output() next: EventEmitter<any> = new EventEmitter<any>();
  @Output() back: EventEmitter<any> = new EventEmitter<any>();
  @Output() finish: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  stepSelected: EventEmitter<WizardStep> = new EventEmitter<WizardStep>();
  @Input()
  set manualMode(manualEnable: boolean) {
    this._wizardService.setManualMode(manualEnable);
  }
  @Input()
  set currentStep(stepId: number) {
    this._wizardService.setCurrentStep(stepId);
  }
  @Input()
  set steps(steps: WizardStep[]) {
    if (typeof steps !== 'undefined' && steps !== null && steps.length > 0) {
      this._wizardService.registerSteps(steps);
    }
  }
  private _subWizardService: Subscription;

  constructor(private _wizardService: WizardService) {}

  ngOnInit() {
    const $1 = this._wizardService.$clickEvents.pipe(tap(e => {
      switch (e) {
        case WizardNavClickEvent.Next:
          this.next.emit();
          break;
        case WizardNavClickEvent.Back:
          this.back.emit();
          break;
        case WizardNavClickEvent.Finish:
          this.finish.emit();
          break;
      }
    }));
    const $2 = this._wizardService.$stepSelected.pipe(tap(e =>
      this.stepSelected.emit(e)
    ));
    this._subWizardService = merge($1, $2).subscribe();
  }

  ngOnDestroy(): void {
    this._subWizardService.unsubscribe();
  }
}
