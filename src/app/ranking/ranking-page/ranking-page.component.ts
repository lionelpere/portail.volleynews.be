import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { DisctrictDDL } from '../../models/ApiResultModels';
import { FvwbApiService } from '../../services/fvwb-api.service';
import { FvwbQuery, FvwbQueryType, FvwbQueryResult } from '../../models/ApiQueryDescriptor';
import { Subscription, combineLatest, BehaviorSubject, merge, empty, of } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-ranking-page',
  templateUrl: './ranking-page.component.html',
  styleUrls: ['./ranking-page.component.scss']
})
export class RankingPageComponent implements OnInit, OnDestroy {
  public districtList: DisctrictDDL[] = [];
  private _subscriptions: Subscription[] = [];
  public _queryType: FvwbQueryType;
  public fvwbQueryType: typeof FvwbQueryType = FvwbQueryType;

  @Input()
  public set queryType(queryType: number) {
    this._queryType = queryType;
  }

  constructor(private apiService: FvwbApiService) {}

  ngOnInit() {
    const fvwbQuery = <FvwbQuery>{
      type: FvwbQueryType.GetDistrictDDL
    };

    const $districts = this.apiService.queryFvwbApi<FvwbQueryResult>(fvwbQuery).pipe(
      tap(d => {
        this.districtList = (<DisctrictDDL[]>d.Data).filter(dis => dis.Id !== 0);
      })
    );

    this._subscriptions = [...this._subscriptions, $districts.subscribe()];
  }

  public ngOnDestroy(): void {
    for (const sub of this._subscriptions) {
      sub.unsubscribe();
    }
  }
}
