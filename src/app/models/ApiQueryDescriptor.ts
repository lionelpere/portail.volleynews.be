import * as moment from 'moment';

export interface FvwbQueryFilter {
  districtId: number;
  championshipId: number;
  clubId: number;
  teamId: number;
  dateFrom: moment.Moment;
  dateTo: moment.Moment;
}

// tslint:disable-next-line:no-empty-interface
export interface FvwbDataFilter extends FvwbQueryFilter {}

export interface FvwbQuery {
  type: FvwbQueryType;
  filter: FvwbQueryFilter;
}

export interface FvwbQueryResult {
  resultDate: Date;
  Data: any;
}

export enum FvwbQueryType {
  GetDistrictDDL, // Get list of districts
  GetClubDLL, // Get list of clubs
  GetTeamDLL, // Get list of teams
  PagingRankingChampionship, // Get list of championships
  PagingRankingReserve, // Get reserve ranking
  PagingRanking, // Get ranking
  PagingSynopticTable, // Get synoptic table
  PagingCalendar
}
