import { FvwbQueryResult } from './ApiQueryDescriptor';

export interface DDLObject {
  Id: number;
  Litteral: string;
  Value: string;
}

// tslint:disable-next-line:no-empty-interface
export interface DisctrictDDL extends DDLObject {}

// tslint:disable-next-line:no-empty-interface
export interface ClubDDL extends DDLObject {}

// tslint:disable-next-line:no-empty-interface
export interface TeamDDL extends DDLObject {}

export interface PagingRankingChampionshipResult extends FvwbQueryResult {
  Data: ChampionshipData[];
  Total: number;
  AggregateResults: string;
  Errors: string;
}

export interface ChampionshipData {
  ChampionshipId: number; // 198,
  Nom: string; // "Nationale 2 Dames",
  Division: string; // "Nationale 2 Dames",
  WithReserve: boolean;
}

export interface PagingRankingResult extends FvwbQueryResult {
  Data: PagingRankingData[];
  Total: number;
  AggregateResults: string;
  Errors: string;
}

export interface PagingRankingData {
  DistrictId: number; // 1
  Province: string; // BWBC
  ProvinceName: string; // BWBC
  ChampionshipId: number; // 210
  TeamId: number; // 2848
  Nom: string; // Provinciale 1 Messieurs
  Position: number; // 1
  Division: string; // ¨rovinciale 1 Messieurs
  SubDivision: string; // null
  ClubShortName: string; // BEVC
  TeamName: string; // Bruxelles Est VC 3
  MatchPlayed: number; // 5
  Winner: number; // 5
  Winner3Points: number; // 5
  Winner2Points: number; // 0
  Looser1Point: number; // 0
  Looser0Point: number; // 0
  Looser: number; // 0
  Forfeit: number; // 0
  ScoreFor: number; // 15
  ScoreAgainst: number; // 2
  Points: number; // 15
  PointsMinus: number; // 0
  ScoreDifference: number; // 13
  Coefficient: number; // 7.5
}

export interface PagingSynopticTableResult extends FvwbQueryResult {
  Data: PagingSynopticTableData[];
  Total: number;
  AggregateResults: string;
  Errors: string;
}

export interface PagingSynopticTableData {
  Equipes: string; // Team name
  // Others properties are codes of the teams (dynamic)
}

export interface PagingCalendarResult extends FvwbQueryResult {
  Data: PagingCalendarData[];
  Total: number;
  AggregateResults: string;
  Errors: string;
}

export interface PagingCalendarData {
  Address: string;
  AwayTeam: string;
  CalendarDate: string;
  CalendarDateString: String;
  CalendarDateDate: Date;
  CalendarHour: string;
  CalendarHourString: string;
  CalendarHourDate: Date;
  CalendarNumber: string;
  ChampionshipCalendarId: number;
  ChampionshipId: number;
  ChampionshipNumber: string;
  City: string;
  DistrictId: 3;
  Division: string;
  HomeClub: string;
  HomeTeam: string;
  Nom: string;
  Number: string;
  PostCode: string;
  Province: string;
  ProvinceName: string;
  Score: string;
  ScoreDetail: string;
  ScoreRes: string;
  Statut: string;
  StatutReserve: string;
  SubDivision: string;
  TrainingPlace: string;
  WithReserve: boolean;
}
