import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr);

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatGridListModule,
  MatCardModule,
  MatMenuModule,
  MatIconModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatListModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatTabsModule
} from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NotImplementedPageComponent } from './components/pages/not-implemented-page/not-implemented-page.component';
import { AppRoutingModule } from './app-routing.module';
import { NotFoundPageComponent } from './components/pages/not-found-page/not-found-page.component';
import { ToolboxUiModule } from './toolbox-ui/toolbox-ui.module';
import { DatePipe, registerLocaleData } from '@angular/common';
import { RankingPageComponent } from './ranking/ranking-page/ranking-page.component';
import { MainContentComponent } from 'src/app/components/pages/main-content-page/main-content-page.component';
import { HomeDashboardComponent } from 'src/app/components/pages/home-dashboard/home-dashboard.component';
import { CalendarWidgetComponent } from 'src/app/components/widgets/calendar-widget/calendar-widget.component';
import { RankingWidgetComponent } from 'src/app/components/widgets/ranking-widget/ranking-widget.component';
import { DistrictSelectorComponent } from 'src/app/components/selectors/district-selector/district-selector.component';
import { ChampionshipSelectorComponent } from 'src/app/components/selectors/championship-selector/championship-selector.component';
import { DocumentationWidgetComponent } from 'src/app/components/widgets/documentation-widget/documentation-widget.component';
import { GridRankingComponent } from 'src/app/components/grids/grid-ranking/grid-ranking.component';
import { GridCalendarComponent } from 'src/app/components/grids/grid-calendar/grid-calendar.component';
import { DisclaimerWidgetComponent } from 'src/app/components/widgets/disclaimer-widget/disclaimer-widget.component';
import { GridSynopticComponent } from './components/grids/grid-synoptic/grid-synoptic.component';
import { LinkPageComponent } from './components/pages/link-page/link-page.component';
import { SynopticTableWidgetComponent } from './components/widgets/synoptic-table-widget/synoptic-table-widget.component';
import { LocalStorageModule } from 'angular-2-local-storage';
import { AboutPageComponent } from './components/pages/about-page/about-page.component';

@NgModule({
  declarations: [
    AppComponent,
    DistrictSelectorComponent,
    GridCalendarComponent,
    ChampionshipSelectorComponent,
    NotImplementedPageComponent,
    NotFoundPageComponent,
    GridRankingComponent,
    RankingPageComponent,
    MainContentComponent,
    HomeDashboardComponent,
    CalendarWidgetComponent,
    RankingWidgetComponent,
    DocumentationWidgetComponent,
    DisclaimerWidgetComponent,
    GridSynopticComponent,
    LinkPageComponent,
    SynopticTableWidgetComponent,
    AboutPageComponent
  ],
  imports: [
    ToolboxUiModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatTabsModule,
    MatCardModule,
    MatTableModule,
    MatGridListModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    LocalStorageModule.withConfig({
      prefix: 'portail-volleynews',
      storageType: 'localStorage'
    })
  ],
  providers: [DatePipe, { provide: LOCALE_ID, useValue: 'fr-FR' }],
  bootstrap: [AppComponent]
})
export class AppModule {}
