import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpUrlEncodingCodec, HttpResponse } from '@angular/common/http';
import * as crypto from 'crypto-js';
import {
  FvwbQuery,
  FvwbQueryType,
  FvwbQueryFilter,
  FvwbQueryResult
} from '../models/ApiQueryDescriptor';
import { Observable, Subject, of } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FvwbApiService {
  private readonly _ajaxProxyUrl = environment.proxyUrl;
  private readonly _useAjaxProxy: boolean = environment.production;

  private readonly _queryUrlIndex = new Map<FvwbQueryType, string>();
  private readonly _queryGetMethodIndex = new Map<FvwbQueryType, boolean>();
  private readonly _queryResultIndex: { [queryHash: string]: FvwbQueryResult } = {};

  constructor(private http: HttpClient) {
    this._queryUrlIndex.set(
      FvwbQueryType.GetDistrictDDL,
      'https://www.portailfvwb.be/Portail/GetDistrictDDL'
    );
    this._queryGetMethodIndex.set(FvwbQueryType.GetDistrictDDL, true);
    this._queryUrlIndex.set(
      FvwbQueryType.PagingRankingChampionship,
      'https://www.portailfvwb.be/Portail/PagingRankingChampionship'
    );

    // Paging Calendar
    this._queryGetMethodIndex.set(FvwbQueryType.PagingRankingChampionship, false);
    this._queryUrlIndex.set(
      FvwbQueryType.PagingCalendar,
      'https://www.portailfvwb.be/Portail/PagingCalendar'
    );
    this._queryGetMethodIndex.set(FvwbQueryType.PagingCalendar, false);
    // Ranking
    this._queryUrlIndex.set(
      FvwbQueryType.PagingRanking,
      'https://www.portailfvwb.be/Portail/PagingRanking'
    );
    this._queryGetMethodIndex.set(FvwbQueryType.PagingRanking, false);
    // Ranking reserve
    this._queryUrlIndex.set(
      FvwbQueryType.PagingRankingReserve,
      'https://www.portailfvwb.be/Portail/PagingRankingReserve'
    );
    this._queryGetMethodIndex.set(FvwbQueryType.PagingRankingReserve, false);
    // Synpotic table
    this._queryUrlIndex.set(
      FvwbQueryType.PagingSynopticTable,
      'https://www.portailfvwb.be/Portail/PagingSynopticTable'
    );
    this._queryGetMethodIndex.set(FvwbQueryType.PagingRankingReserve, false);
  }

  queryFvwbApi<T extends FvwbQueryResult>(query: FvwbQuery): Observable<T> {
    console.log('Execute FvwbQuery', query);
    const cachedResult: T = <T>this.getQueryResult(query);
    if (cachedResult && cachedResult.resultDate) {
      console.log('cachedResultFound => ', cachedResult);
      return of(cachedResult);
    }

    let url = this._queryUrlIndex.get(query.type);
    const isGet = this._queryGetMethodIndex.get(query.type);

    // console.log('Query url',url);
    const urlParams: string = this.getParams(query.filter);
    if (urlParams) {
      url = url + urlParams;
    }

    let httpMethod = null;
    let httpOptions = null;

    if (this._useAjaxProxy) {
      httpOptions = {
        observe: 'response',
        headers: new HttpHeaders({
          'X-Proxy-URL': new HttpUrlEncodingCodec().encodeValue(url)
        })
      };
      url = this._ajaxProxyUrl;
    } else {
      httpOptions = {
        observe: 'response'
      };
    }

    if (isGet) {
      // console.log('Query method [GET]', url);
      // console.log('Query method [GET-OPTIONS]', httpOptions);
      httpMethod = this.http.get<T>(url, httpOptions);
    } else {
      // console.log('Query method [POST]', url);
      // console.log('Query method [POST-OPTIONS]', httpOptions);
      httpMethod = this.http.post<T>(url, null, httpOptions);
    }

    return httpMethod.pipe(
      tap(() => {}),
      map(res => {
        if (
          query.type === FvwbQueryType.GetDistrictDDL ||
          query.type === FvwbQueryType.GetClubDLL ||
          query.type === FvwbQueryType.GetTeamDLL
        ) {
          // For DDL query we artificially create a 'standart' query result
          return <FvwbQueryResult>{
            Data: (<HttpResponse<T>>res).body
          };
        }
        return (<HttpResponse<T>>res).body;
      }),
      tap(result => this.saveQueryResult(query, <T>result))
    );
  }

  private getParams(fvwbFilter: FvwbQueryFilter): string {
    let urlParams = '';
    let first = true;

    if (!!fvwbFilter) {
      for (const property in fvwbFilter) {
        if (fvwbFilter[property]) {
          if (first) {
            urlParams = urlParams + '?';
            first = false;
          } else {
            urlParams = urlParams + '&';
          }
          urlParams = urlParams + property + '=' + fvwbFilter[property];
        }
      }
    }
    return urlParams;
  }

  protected saveQueryResult(query: FvwbQuery, results: FvwbQueryResult) {
    const queryHash = this.getHashCode(query) || '*';
    results.resultDate = new Date();
    this._queryResultIndex[queryHash] = results;
    console.log('saveQueryResult -> queryHash: ', queryHash);
  }

  protected getQueryResult<T extends FvwbQueryResult>(dataFilter: FvwbQuery): T {
    const queryHash = this.getHashCode(dataFilter) || '*';
    const result: T = <T>this._queryResultIndex[queryHash] || <T>{};
    if (result && result.resultDate) {
      const dtNow: Date = new Date();
      const elapsedTimeInMs: any = dtNow.getTime() - result.resultDate.getTime();
      if (elapsedTimeInMs < 300000) {
        return result;
      }
    }
    return null;
  }

  private getHashCode(object: any): string {
    const type = typeof object;
    let serializedCode = '';
    try {
      if (type === 'object') {
        if (object.hasOwnProperty('_isAMomentObject')) {
          serializedCode += `[${type}:${this.getHashCode(object.format('YYYY-MM-DD'))}]`;
        } else {
          for (const key in object) {
            if (object[key]) {
              serializedCode += `[${type}:${this.getHashCode(object[key])}]`;
            }
          }
        }
      } else if (type === 'function') {
        serializedCode += `[${type}:${object.ToString()}]`;
      } else {
        serializedCode += `[${type}:${object}]`;
      }
    } catch (err) {}

    serializedCode = serializedCode.replace(/\s/g, '');
    const hash = !!serializedCode ? crypto.MD5(serializedCode).toString() : null;
    return hash;
  }
}
