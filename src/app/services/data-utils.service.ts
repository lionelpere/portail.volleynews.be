import { Injectable, Inject, LOCALE_ID } from '@angular/core';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class DataUtilsService {
  constructor(private datePipe: DatePipe, @Inject(LOCALE_ID) private locale: string) {}

  public getShortChampionshipName(fullName: string): string {
    if (!!fullName) {
      fullName = fullName.replace(new RegExp(' ', 'g'), '');
      fullName = fullName.replace('Nationale', 'N');
      fullName = fullName.replace('Provinciale', 'P');
      fullName = fullName.replace('Dames', 'D');
      fullName = fullName.replace('Messieurs', 'M');
      fullName = fullName.replace('Filles', 'F');
      fullName = fullName.replace('Garçons', 'G');
      fullName = fullName.replace('U-Cadettes', 'Cadettes');
      fullName = fullName.replace('U-Cadets', 'Cadets');
      fullName = fullName.replace('U-Minimes', 'Minimes');
      fullName = fullName.replace('U-Pupilles', 'Pupilles');
      fullName = fullName.replace('Garçons', 'G');
      fullName = fullName.replace('Ligue', 'L');
      fullName = fullName.replace('Série', '');
      fullName = fullName.replace('série', '');
    }
    return fullName;
  }

  public getDateFromString(stringDate: string): Date {
    stringDate = stringDate.replace('/Date(', '');
    stringDate = stringDate.replace('/', '');
    stringDate = stringDate.replace(')', '');
    return new Date(parseFloat(stringDate));
  }

  public getFrenchDateStringFromDate(date: Date): string {
    return this.datePipe.transform(date, 'dd/MM/yyyy');
  }

  public getHourStringFromDate(date: Date): string {
    return this.datePipe.transform(date, 'HH:mm');
  }

  /**
   * Methods returning formatted date from fuck*** fvwb string date :p
   */

  public getFrenchDateStringFromString(stringDate: string): string {
    return this.getFrenchDateStringFromDate(this.getDateFromString(stringDate));
  }

  public getHourStringFromString(stringDate: string): string {
    return this.getHourStringFromDate(this.getDateFromString(stringDate));
  }
}
